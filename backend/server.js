const app = require("./app");
// const path = require("path");
const http = require("http");
const Db = require("./db/database");

const normalizePort = (val) => {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
};

const onError = (error) => {
  if (error.syscall !== "listen") {
    throw error;
  }
  const bind = typeof addr === "string" ? "pipe " + addr : "port " + port;
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
};

const onListening = () => {
  const addr = server.address();
  const bind = typeof addr === "string" ? "pipe " + addr : "port " + port;
  console.log(`Server is listening on ${bind}...`);
};

const port = normalizePort(process.env.PORT || "3000");
app.set("port", port);

const server = http.createServer(app);
server.on("error", onError);
server.on("listening", onListening);

try {
  (async () => {
    // const syncResult = await Db.sequelize.sync();

    // const syncResult = await Db.sequelize.sync({ force: true });
    // await Db.ProductCategoryModel.bulkCreate([
    //   {
    //     name: "Блоки питания для ноутбуков",
    //     imageUrl: "assets/images/bloki-pitaniya.webp",
    //   },
    //   { name: "Пульты ДУ", imageUrl: "assets/images/tv-remote.webp" },
    //   { name: "Радиодетали", imageUrl: "assets/images/components.webp" },
    //   {
    //     name: "Транзисторы",
    //     imageUrl: "assets/images/transistors.webp",
    //     parentId: 3,
    //   },
    //   {
    //     name: "Резисторы",
    //     imageUrl: "assets/images/resistors.webp",
    //     parentId: 3,
    //   },
    //   {
    //     name: "Конденсаторы",
    //     imageUrl: "assets/images/capacitors.webp",
    //     parentId: 3,
    //   },
    // ]);

    // await Db.ManufacturerModel.bulkCreate([
    //   { name: "Fairchild" },
    //   { name: "ON Semiconductor" },
    //   { name: "Samsung" },
    //   { name: "LG" },
    //   { name: "Asus" },
    //   { name: "HP" },
    //   { name: "Sony" },
    //   { name: "Panasonic" },
    //   { name: "Ohmite" },
    //   { name: "Diodes Inc." },
    //   { name: "Vishay" },
    //   { name: "Toshiba" },
    // ]);

    // const product1 = await Db.ProductModel.create({
    //   name: "FQP50N06",
    //   price: 80,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/50n06.webp",
    //   available: 52,
    //   description: "MOSFET, N-канал, 60В, 50А",
    //   manufacturerId: 1,
    // });
    // product1.addProductCategories([4]);

    // const product2 = await Db.ProductModel.create({
    //   name: "FQP30N06",
    //   price: 30,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/30n06.webp",
    //   available: 79,
    //   description: "MOSFET, N-канал, 60В, 30А",
    //   manufacturerId: 2,
    // });
    // product2.addProductCategories([4]);

    // const product3 = await Db.ProductModel.create({
    //   name: "ECH8310",
    //   price: 30,
    //   discountPrice: 25,
    //   imageUrl: "assets/images/ech8310.webp",
    //   available: 108,
    //   description: "MOSFET, P-канал, 30В, 9А",
    //   manufacturerId: 2,
    // });
    // product3.addProductCategories([4]);

    // const product4 = await Db.ProductModel.create({
    //   name: "FQP30N06",
    //   price: 30,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/transistors.webp",
    //   available: 0,
    //   description: "MOSFET, N-канал, 60В, 30А",
    //   manufacturerId: 2,
    // });
    // product4.addProductCategories([4]);

    // const product5 = await Db.ProductModel.create({
    //   name: "Samsung AA59-00607A",
    //   price: 450,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/aa5900607a.webp",
    //   available: 2,
    //   description: "Подходит почти ко всем LED/LCD-TV от Samsung",
    //   manufacturerId: 3,
    // });
    // product5.addProductCategories([2]);

    // const product5a = await Db.ProductModel.create({
    //   name: "Sony KDL46R450A",
    //   price: 800,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/kdl46r450a.webp",
    //   available: 1,
    //   description: "Оригинальный пульт ДУ для Sony KDL46R450A",
    //   manufacturerId: 7,
    // });
    // product5a.addProductCategories([2]);

    // const product6 = await Db.ProductModel.create({
    //   name: "LG AKB73715646",
    //   price: 380,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/akb73715646.webp",
    //   available: 1,
    //   description: "Для моделей AKB73715646, AKB73715601, AKB73715634",
    //   manufacturerId: 4,
    // });
    // product6.addProductCategories([2]);

    // const product6a = await Db.ProductModel.create({
    //   name: "Samsung BN59-01315A",
    //   price: 700,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/bn59-01315a.webp",
    //   available: 1,
    //   description: "Пульт ДУ для Smart TV от Samsung",
    //   manufacturerId: 3,
    // });
    // product6a.addProductCategories([2]);

    // const product6b = await Db.ProductModel.create({
    //   name: "Panasonic N2QAYB000604",
    //   price: 880,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/n2qayb000604.webp",
    //   available: 1,
    //   description: "Для телевизоров серий THP, THL",
    //   manufacturerId: 8,
    // });
    // product6b.addProductCategories([2]);

    // const product6c = await Db.ProductModel.create({
    //   name: "Toshiba SE-R0144",
    //   price: 1200,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/se_r0144.webp",
    //   available: 1,
    //   description: "Оригинальный пульт Toshiba SE-R0144",
    //   manufacturerId: 12,
    // });
    // product6c.addProductCategories([2]);

    // const product7 = await Db.ProductModel.create({
    //   name: "2N3904",
    //   price: 20,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/2n3904.webp",
    //   available: 27,
    //   description: "BJT, NPN, 40 В, 0.2 А, 0.625 Вт",
    //   manufacturerId: 2,
    // });
    // product7.addProductCategories([4]);

    // const product8 = await Db.ProductModel.create({
    //   name: "2N3906",
    //   price: 15,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/2n3906.webp",
    //   available: 45,
    //   description: "BJT, PNP, 40 В, 0.2 А, 0.625 Вт",
    //   manufacturerId: 2,
    // });
    // product8.addProductCategories([4]);

    // const product9 = await Db.ProductModel.create({
    //   name: "Блок питания Amperin AI-AS145",
    //   price: 1490,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/amperin-ai-as145.webp",
    //   available: 2,
    //   description: "Блок питания AmperIn для ноутбука Asus",
    //   manufacturerId: 5,
    // });
    // product9.addProductCategories([1]);

    // const product10 = await Db.ProductModel.create({
    //   name: "Блок питания HP",
    //   price: 950,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/bp_hp.webp",
    //   available: 1,
    //   description: "Блок питания для ноутбука HP 19,5V-2,31 (NH)",
    //   manufacturerId: 6,
    // });
    // product10.addProductCategories([1]);

    // const product11 = await Db.ProductModel.create({
    //   name: "TIP120",
    //   price: 75,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/tip120.webp",
    //   available: 15,
    //   description: "BJT, NPN, 60 В, 5 А",
    //   manufacturerId: 1,
    // });
    // product11.addProductCategories([4]);

    // const product11a = await Db.ProductModel.create({
    //   name: "TIP31A",
    //   price: 55,
    //   discountPrice: 40,
    //   imageUrl: "assets/images/tip31a.webp",
    //   available: 21,
    //   description: "BJT, NPN, 60 В, 5 А",
    //   manufacturerId: 2,
    // });
    // product11a.addProductCategories([4]);

    // const product11b = await Db.ProductModel.create({
    //   name: "TIP32B",
    //   price: 65,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/tip32b.webp",
    //   available: 19,
    //   description: "BJT, PNP, 80 В, 5 А",
    //   manufacturerId: 1,
    // });
    // product11b.addProductCategories([4]);

    // const product11c = await Db.ProductModel.create({
    //   name: "ZVN2110A",
    //   price: 50,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/zvn2110a.webp",
    //   available: 0,
    //   description: "MOSFET, N-канал, 100В, 0.32А",
    //   manufacturerId: 10,
    // });
    // product11c.addProductCategories([4]);

    // const product11d = await Db.ProductModel.create({
    //   name: "ZVP2110A",
    //   price: 50,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/zvp2110a.webp",
    //   available: 6,
    //   description: "MOSFET, P-канал, 100В, 0.23А",
    //   manufacturerId: 10,
    // });
    // product11d.addProductCategories([4]);

    // const product11e = await Db.ProductModel.create({
    //   name: "IRF510",
    //   price: 90,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/irf510.webp",
    //   available: 10,
    //   description: "MOSFET, N-канал, 100В, 5.6А",
    //   manufacturerId: 11,
    // });
    // product11e.addProductCategories([4]);

    // const product12 = await Db.ProductModel.create({
    //   name: "Резистор 100 Ом",
    //   price: 10,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/resistor.webp",
    //   available: 80,
    //   manufacturerId: 8,
    // });
    // product12.addProductCategories([5]);

    // const product13 = await Db.ProductModel.create({
    //   name: "Резистор 10 кОм",
    //   price: 15,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/resistor.webp",
    //   available: 125,
    //   manufacturerId: 8,
    // });
    // product13.addProductCategories([5]);

    // const product14 = await Db.ProductModel.create({
    //   name: "Резистор 470 Ом",
    //   price: 10,
    //   discountPrice: 0,
    //   imageUrl: "assets/images/resistor.webp",
    //   available: 56,
    //   manufacturerId: 9,
    // });
    // product14.addProductCategories([5]);

    // await Db.PriceListCategoryModel.bulkCreate([
    //   {
    //     name: "Ремонт ноутбуков",
    //     imageUrl: "",
    //   },
    //   {
    //     name: "Ремонт компьютеров",
    //     imageUrl: "",
    //   },
    // ]);

    // await Db.PriceListItemModel.bulkCreate([
    //   {
    //     name: "Диагностика ноутбука",
    //     price: 300,
    //     isPriceFrom: false,
    //     priceListCategoryId: 1,
    //   },
    //   {
    //     name: "Установка драйверов оборудования",
    //     price: 300,
    //     isPriceFrom: true,
    //     priceListCategoryId: 1,
    //   },
    //   {
    //     name: "Установка и настройка ПО",
    //     price: 500,
    //     isPriceFrom: true,
    //     priceListCategoryId: 1,
    //   },
    //   {
    //     name: "Лечение от вирусов и вредоносного ПО",
    //     price: 500,
    //     isPriceFrom: false,
    //     priceListCategoryId: 1,
    //   },
    //   {
    //     name: "Замена ЖК-дисплея",
    //     price: 1000,
    //     isPriceFrom: false,
    //     priceListCategoryId: 1,
    //   },
    //   {
    //     name: "Диагностика системного блока",
    //     price: 300,
    //     isPriceFrom: true,
    //     priceListCategoryId: 2,
    //   },
    //   {
    //     name: "Диагностика материнской платы",
    //     price: 300,
    //     isPriceFrom: true,
    //     priceListCategoryId: 2,
    //   },
    //   {
    //     name: "Диагностика процессора",
    //     price: 300,
    //     isPriceFrom: true,
    //     priceListCategoryId: 2,
    //   },
    //   {
    //     name: "Диагностика жесткого диска",
    //     price: 300,
    //     isPriceFrom: true,
    //     priceListCategoryId: 2,
    //   },
    //   {
    //     name: "Устранение неполадок в операционной системе",
    //     price: 500,
    //     isPriceFrom: true,
    //     priceListCategoryId: 2,
    //   },
    //   {
    //     name: "Замена компонентов системного блока",
    //     price: 1000,
    //     isPriceFrom: false,
    //     priceListCategoryId: 2,
    //   },
    // ]);

    // const attrG1 = await Db.AttrGroupModel.create({
    //   name: "Общие характеристики",
    //   productCategoryId: 4,
    // });
    // const attrG2 = await Db.AttrGroupModel.create({
    //   name: "Статические характеристики",
    //   productCategoryId: 4,
    // });
    // const attrG3 = await Db.AttrGroupModel.create({
    //   name: "Общие характеристики",
    //   productCategoryId: 1,
    // });
    // const attrG4 = await Db.AttrGroupModel.create({
    //   name: "Общие характеристики",
    //   productCategoryId: 5,
    // });

    // const attr1 = await Db.AttributeModel.create({
    //   name: "Тип транзистора",
    //   attributeGroupId: 1,
    // });
    // const attr2 = await Db.AttributeModel.create({
    //   name: "Структура",
    //   attributeGroupId: 1,
    // });
    // const attr3 = await Db.AttributeModel.create({
    //   name: "Выходное напряжение",
    //   attributeGroupId: 3,
    // });
    // const attr4 = await Db.AttributeModel.create({
    //   name: "Номинальное сопротивление",
    //   attributeGroupId: 4,
    // });
    // const attr5 = await Db.AttributeModel.create({
    //   name: "Точность, %",
    //   attributeGroupId: 4,
    // });
    // const attr6 = await Db.AttributeModel.create({
    //   name: "Номинальная мощность",
    //   attributeGroupId: 4,
    // });

    // await product1.addAttribute(attr1, { through: { value: "Полевой" } });
    // await product1.addAttribute(attr2, { through: { value: "N-канал" } });

    // await product2.addAttribute(attr1, { through: { value: "Полевой" } });
    // await product2.addAttribute(attr2, { through: { value: "N-канал" } });

    // await product3.addAttribute(attr1, { through: { value: "Полевой" } });
    // await product3.addAttribute(attr2, { through: { value: "P-канал" } });

    // await product4.addAttribute(attr1, { through: { value: "Полевой" } });
    // await product4.addAttribute(attr2, { through: { value: "N-канал" } });

    // await product7.addAttribute(attr1, { through: { value: "Биполярный" } });
    // await product7.addAttribute(attr2, { through: { value: "NPN" } });

    // await product8.addAttribute(attr1, { through: { value: "Биполярный" } });
    // await product8.addAttribute(attr2, { through: { value: "PNP" } });

    // await product11.addAttribute(attr1, { through: { value: "Биполярный" } });
    // await product11.addAttribute(attr2, { through: { value: "NPN" } });

    // await product11a.addAttribute(attr1, { through: { value: "Биполярный" } });
    // await product11a.addAttribute(attr2, { through: { value: "NPN" } });

    // await product11b.addAttribute(attr1, { through: { value: "Биполярный" } });
    // await product11b.addAttribute(attr2, { through: { value: "PNP" } });

    // await product11c.addAttribute(attr1, { through: { value: "Полевой" } });
    // await product11c.addAttribute(attr2, { through: { value: "N-канал" } });

    // await product11d.addAttribute(attr1, { through: { value: "Полевой" } });
    // await product11d.addAttribute(attr2, { through: { value: "P-канал" } });

    // await product11e.addAttribute(attr1, { through: { value: "Полевой" } });
    // await product11e.addAttribute(attr2, { through: { value: "N-канал" } });

    // await product9.addAttribute(attr3, { through: { value: "19 В" } });
    // await product10.addAttribute(attr3, { through: { value: "19.5 В" } });

    // await product12.addAttribute(attr4, { through: { value: "100 Ом" } });
    // await product13.addAttribute(attr4, { through: { value: "10 кОм" } });
    // await product14.addAttribute(attr4, { through: { value: "470 Ом" } });

    // await product12.addAttribute(attr5, { through: { value: "1" } });
    // await product13.addAttribute(attr5, { through: { value: "5" } });
    // await product14.addAttribute(attr5, { through: { value: "5" } });

    // await product12.addAttribute(attr6, { through: { value: "0.125 Вт" } });
    // await product13.addAttribute(attr6, { through: { value: "1 Вт" } });
    // await product14.addAttribute(attr6, { through: { value: "0.125 Вт" } });

    server.listen(port);
  })();
} catch (err) {
  console.error(err);
}

module.exports = server;
