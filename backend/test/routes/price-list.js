process.env.NODE_ENV = "test";

const fs = require("fs");
const path = require("path");
const chai = require("chai");
const server = require("../../server");
const should = chai.should();

chai.use(require("chai-http"));
chai.use(require("chai-json-schema"));

//Load JSON-schemas
const priceListSchema = JSON.parse(
  fs.readFileSync(path.join(__dirname, "../../schemas/price-list.json"))
);
const priceListCategorySchema = JSON.parse(
  fs.readFileSync(
    path.join(__dirname, "../../schemas/price-list-category.json")
  )
);

//Add schemas to validator to use $ref correctly
chai.tv4.addSchema("/schemas/price-list.json", priceListSchema);
chai.tv4.addSchema(
  "/schemas/price-list-category.json",
  priceListCategorySchema
);

describe("GET /full", () => {
  it("should get full price list", (done) => {
    chai
      .request(server)
      .get("/api/price-list/full")
      .end((err, res) => {
        if (err) done(err);
        res.should.have.status(200);
        res.body.should.be.jsonSchema(priceListSchema);

        res.body.length.should.be.eql(2);
        res.body[0].priceListItems.length.should.be.eql(5);
        res.body[1].priceListItems.length.should.be.eql(6);
        done();
      });
  });
});

describe("GET /category/:id", () => {
  it("should get 1st category of price-list", (done) => {
    chai
      .request(server)
      .get("/api/price-list/category/1")
      .end((err, res) => {
        if (err) done(err);
        res.should.have.status(200);
        res.body.should.be.jsonSchema(priceListCategorySchema);

        res.body.id.should.be.eql(1);
        res.body.name.should.be.eql("Ремонт ноутбуков");
        res.body.priceListItems.length.should.be.eql(5);
        done();
      });
  });
});

describe("GET /category/:id", () => {
  it("should get status 404 when categoryId is wrong", (done) => {
    chai
      .request(server)
      .get("/api/price-list/category/7")
      .end((err, res) => {
        if (err) done(err);
        res.should.have.status(404);
        done();
      });
  });
});
