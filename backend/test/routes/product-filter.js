process.env.NODE_ENV = "test";

const fs = require("fs");
const path = require("path");
const chai = require("chai");
const server = require("../../server");
const should = chai.should();

const TestUtils = require("../../utils/test-utils");

chai.use(require("chai-http"));
chai.use(require("chai-json-schema"));
chai.use(require("chai-things"));

//Load JSON-schemas
const attributeSchema = JSON.parse(
  fs.readFileSync(path.join(__dirname, "../../schemas/attribute.json"))
);
const productSchema = JSON.parse(
  fs.readFileSync(path.join(__dirname, "../../schemas/product.json"))
);
const productFromCategorySchema = JSON.parse(
  fs.readFileSync(
    path.join(__dirname, "../../schemas/products-from-category.json")
  )
);

//Add schemas to validator to use $ref correctly
chai.tv4.addSchema("/schemas/attribute.json", attributeSchema);
chai.tv4.addSchema("/schemas/product.json", productSchema);
chai.tv4.addSchema(
  "/schemas/products-from-category.json",
  productFromCategorySchema
);

verifyResponse = (res, categoryId) => {
  res.should.have.status(200);
  res.body.should.be.jsonSchema(productFromCategorySchema);
  res.body.id.should.be.eql(categoryId);
};

describe("GET /filter", () => {
  it("should get all products from category if filter data is empty", (done) => {
    const testFilter = {
      page: 1,
      itemsperpage: 5,
      categoryId: 4,
      filterData: [],
      itemsperpage: 5,
    };
    console.log("getParamsFromFilterCriteria");
    console.log(TestUtils.getParamsFromFilterCriteria(testFilter));
    chai
      .request(server)
      .get("/api/product/filter")
      .query(TestUtils.getParamsFromFilterCriteria(testFilter))
      .end((err, res) => {
        if (err) done(err);
        verifyResponse(res, testFilter.categoryId);
        res.body.totalItems.should.be.eql(12);
        res.body.products.length.should.be.eql(5);
        done();
      });
  });
});

describe("GET /filter", () => {
  it("should get correct products if only 1 manufacturer is selected", (done) => {
    const testManufacturerValues = ["Fairchild"];
    const testFilter = {
      page: 1,
      categoryId: 4,
      filterData: [{ attributeId: 0, values: testManufacturerValues }],
      itemsperpage: 5,
    };

    chai
      .request(server)
      .get("/api/product/filter")
      .query(TestUtils.getParamsFromFilterCriteria(testFilter))
      .end((err, res) => {
        if (err) done(err);
        verifyResponse(res, testFilter.categoryId);
        res.body.totalItems.should.be.eql(3);
        res.body.products.length.should.be.eql(3);
        res.body.products.should.all.have.property(
          "manufacturerName",
          testManufacturerValues[0]
        );
        done();
      });
  });
});

describe("GET /filter", () => {
  it("should get correct products if multiple manufacturers are selected", (done) => {
    const testManufacturerValues = ["Samsung", "Sony"];
    const testFilter = {
      page: 1,
      categoryId: 2,
      filterData: [{ attributeId: 0, values: testManufacturerValues }],
      itemsperpage: 5,
    };

    chai
      .request(server)
      .get("/api/product/filter")
      .query(TestUtils.getParamsFromFilterCriteria(testFilter))
      .end((err, res) => {
        if (err) done(err);
        verifyResponse(res, testFilter.categoryId);
        res.body.totalItems.should.be.eql(3);
        res.body.products.length.should.be.eql(3);
        res.body.products.forEach((product) => {
          (
            product.manufacturerName === testManufacturerValues[0] ||
            product.manufacturerName === testManufacturerValues[1]
          ).should.be.true;
        });
        done();
      });
  });
});

describe("GET /filter", () => {
  it("should get correct products if only 1 attribute value is selected", (done) => {
    const testFilterValues = ["N-канал"];
    const testFilter = {
      page: 1,
      categoryId: 4,
      filterData: [{ attributeId: 2, values: testFilterValues }],
      itemsperpage: 5,
    };

    chai
      .request(server)
      .get("/api/product/filter")
      .query(TestUtils.getParamsFromFilterCriteria(testFilter))
      .end((err, res) => {
        if (err) done(err);
        verifyResponse(res, testFilter.categoryId);
        res.body.totalItems.should.be.eql(5);
        res.body.products.length.should.be.eql(5);

        res.body.products.forEach((product) => {
          product.attributes.should.contain.an.item.with.property(
            "attrValue",
            testFilterValues[0]
          );
        });
        done();
      });
  });
});

describe("GET /filter", () => {
  it("should get correct products if 2 attribute values are selected", (done) => {
    const testFilterValues = ["P-канал", "NPN"];
    const testFilter = {
      page: 1,
      categoryId: 4,
      filterData: [{ attributeId: 2, values: testFilterValues }],
      itemsperpage: 5,
    };

    chai
      .request(server)
      .get("/api/product/filter")
      .query(TestUtils.getParamsFromFilterCriteria(testFilter))
      .end((err, res) => {
        if (err) done(err);
        verifyResponse(res, testFilter.categoryId);
        res.body.totalItems.should.be.eql(5);
        res.body.products.length.should.be.eql(5);

        res.body.products.forEach((product) => {
          (
            product.attributes[1].attrValue === testFilterValues[0] ||
            product.attributes[1].attrValue === testFilterValues[1]
          ).should.be.true;
        });
        done();
      });
  });
});

describe("GET /filter", () => {
  it("should get correct products if 2 different attributes are selected", (done) => {
    const testFilterValues1 = ["Биполярный"];
    const testFilterValues2 = ["N-канал", "NPN"];
    const testFilter = {
      page: 1,
      categoryId: 4,
      filterData: [
        { attributeId: 1, values: testFilterValues1 },
        { attributeId: 2, values: testFilterValues2 },
      ],
      itemsperpage: 5,
    };

    chai
      .request(server)
      .get("/api/product/filter")
      .query(TestUtils.getParamsFromFilterCriteria(testFilter))
      .end((err, res) => {
        if (err) done(err);
        verifyResponse(res, testFilter.categoryId);
        res.body.totalItems.should.be.eql(3);
        res.body.products.length.should.be.eql(3);

        res.body.products.forEach((product) => {
          product.attributes.should.contain.an.item.with.property(
            "attrValue",
            testFilterValues1[0]
          );
          (
            product.attributes[1].attrValue === testFilterValues2[0] ||
            product.attributes[1].attrValue === testFilterValues2[1]
          ).should.be.true;
        });
        done();
      });
  });
});

describe("GET /filter", () => {
  it(`should get correct products if manufacturer and 2 different
  attributes are selected`, (done) => {
    const testManufacturerValues = ["Fairchild"];
    const testFilterValues1 = ["Полевой"];
    const testFilterValues2 = ["N-канал", "NPN"];
    const testFilter = {
      page: 1,
      categoryId: 4,
      filterData: [
        { attributeId: 0, values: testManufacturerValues },
        { attributeId: 1, values: testFilterValues1 },
        { attributeId: 2, values: testFilterValues2 },
      ],
      itemsperpage: 5,
    };

    chai
      .request(server)
      .get("/api/product/filter")
      .query(TestUtils.getParamsFromFilterCriteria(testFilter))
      .end((err, res) => {
        if (err) done(err);
        verifyResponse(res, testFilter.categoryId);
        res.body.totalItems.should.be.eql(1);
        res.body.products.length.should.be.eql(1);
        res.body.products[0].manufacturerName.should.be.eql(
          testManufacturerValues[0]
        );
        res.body.products[0].attributes.should.contain.an.item.with.property(
          "attrValue",
          testFilterValues1[0]
        );
        (
          res.body.products[0].attributes[1].attrValue ===
            testFilterValues2[0] ||
          res.body.products[0].attributes[1].attrValue === testFilterValues2[1]
        ).should.be.true;

        done();
      });
  });
});

describe("GET /filter", () => {
  it("should get correct products if only min price is specified", (done) => {
    const testFilter = {
      page: 1,
      categoryId: 4,
      filterData: [],
      priceRange: { minPrice: 30, maxPrice: 0 },
      itemsperpage: 5,
    };
    chai
      .request(server)
      .get("/api/product/filter")
      .query(TestUtils.getParamsFromFilterCriteria(testFilter))
      .end((err, res) => {
        if (err) done(err);
        verifyResponse(res, testFilter.categoryId);
        res.body.totalItems.should.be.eql(10);
        res.body.products.length.should.be.eql(5);
        res.body.products.forEach((product) => {
          product.price.should.be.at.least(30);
        });
        done();
      });
  });
});

describe("GET /filter", () => {
  it("should get correct products if only max price is specified", (done) => {
    const testFilter = {
      page: 1,
      categoryId: 4,
      filterData: [],
      priceRange: { minPrice: 0, maxPrice: 45 },
      itemsperpage: 5,
    };
    chai
      .request(server)
      .get("/api/product/filter")
      .query(TestUtils.getParamsFromFilterCriteria(testFilter))
      .end((err, res) => {
        if (err) done(err);
        verifyResponse(res, testFilter.categoryId);
        res.body.totalItems.should.be.eql(5);
        res.body.products.length.should.be.eql(5);
        res.body.products.forEach((product) => {
          product.price.should.be.at.most(45);
        });
        done();
      });
  });
});

describe("GET /filter", () => {
  it("should get correct products if both min and max price are specified", (done) => {
    const testFilter = {
      page: 1,
      categoryId: 4,
      filterData: [],
      priceRange: { minPrice: 30, maxPrice: 70 },
      itemsperpage: 5,
    };
    chai
      .request(server)
      .get("/api/product/filter")
      .query(TestUtils.getParamsFromFilterCriteria(testFilter))
      .end((err, res) => {
        if (err) done(err);
        verifyResponse(res, testFilter.categoryId);
        res.body.totalItems.should.be.eql(7);
        res.body.products.length.should.be.eql(5);
        res.body.products.forEach((product) => {
          product.price.should.be.within(30, 70);
        });
        done();
      });
  });
});

describe("GET /filter", () => {
  it("should get correct products if text search query is specified", (done) => {
    const testFilter = {
      page: 1,
      categoryId: 4,
      filterData: [],
      searchQuery: "fqp",
      itemsperpage: 5,
    };
    chai
      .request(server)
      .get("/api/product/filter")
      .query(TestUtils.getParamsFromFilterCriteria(testFilter))
      .end((err, res) => {
        if (err) done(err);
        verifyResponse(res, testFilter.categoryId);
        res.body.totalItems.should.be.eql(3);
        res.body.products.length.should.be.eql(3);
        res.body.products.forEach((product) => {
          product.name.toLowerCase().should.include("fqp");
        });
        done();
      });
  });
});

describe("GET /filter", () => {
  it("should get zero products if no products matching text query were found", (done) => {
    const testFilter = {
      page: 1,
      categoryId: 4,
      filterData: [],
      searchQuery: "asdasdasd",
      itemsperpage: 5,
    };
    chai
      .request(server)
      .get("/api/product/filter")
      .query(TestUtils.getParamsFromFilterCriteria(testFilter))
      .end((err, res) => {
        if (err) done(err);
        verifyResponse(res, testFilter.categoryId);
        res.body.totalItems.should.be.eql(0);
        res.body.products.length.should.be.eql(0);
        done();
      });
  });
});

describe("GET /filter", () => {
  it("should get correct products if text query is combined with additional filters", (done) => {
    const testManufacturerValues = ["ON Semiconductor"];
    const testFilterValues2 = ["N-канал", "P-канал"];
    const testFilter = {
      page: 1,
      categoryId: 4,
      filterData: [
        { attributeId: 0, values: testManufacturerValues },
        { attributeId: 2, values: testFilterValues2 },
      ],
      searchQuery: "fqp",
      itemsperpage: 5,
    };
    chai
      .request(server)
      .get("/api/product/filter")
      .query(TestUtils.getParamsFromFilterCriteria(testFilter))
      .end((err, res) => {
        if (err) done(err);
        verifyResponse(res, testFilter.categoryId);
        res.body.totalItems.should.be.eql(2);
        res.body.products.length.should.be.eql(2);

        res.body.products.forEach((product) => {
          product.manufacturerName.should.be.eql(testManufacturerValues[0]);
          product.name.toLowerCase().should.include("fqp");
          (
            product.attributes[1].attrValue === testFilterValues2[0] ||
            product.attributes[1].attrValue === testFilterValues2[1]
          ).should.be.true;
        });
        done();
      });
  });
});
