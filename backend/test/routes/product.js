process.env.NODE_ENV = "test";

const fs = require("fs");
const path = require("path");
const chai = require("chai");
const server = require("../../server");
const should = chai.should();

chai.use(require("chai-http"));
chai.use(require("chai-json-schema"));

//Load JSON-schemas
const attributeSchema = JSON.parse(
  fs.readFileSync(path.join(__dirname, "../../schemas/attribute.json"))
);
const productSchema = JSON.parse(
  fs.readFileSync(path.join(__dirname, "../../schemas/product.json"))
);
const productFromCategorySchema = JSON.parse(
  fs.readFileSync(
    path.join(__dirname, "../../schemas/products-from-category.json")
  )
);

//Add schemas to validator to use $ref correctly
chai.tv4.addSchema("/schemas/attribute.json", attributeSchema);
chai.tv4.addSchema("/schemas/product.json", productSchema);
chai.tv4.addSchema(
  "/schemas/products-from-category.json",
  productFromCategorySchema
);

describe("GET /:productId", () => {
  it("should get product by its id", (done) => {
    chai
      .request(server)
      .get("/api/product/id/4")
      .end((err, res) => {
        if (err) done(err);
        res.should.have.status(200);
        res.body.should.be.jsonSchema(productSchema);

        res.body.id.should.be.eql(4);
        res.body.attributes.length.should.be.eql(2);
        done();
      });
  });
});

describe("GET /:productId", () => {
  it("should get status 404 if product doesn't exist", (done) => {
    chai
      .request(server)
      .get("/api/product/id/458")
      .end((err, res) => {
        if (err) done(err);
        res.should.have.status(404);
        done();
      });
  });
});

describe("GET /category/:categoryId", () => {
  it("should get all products from category", (done) => {
    chai
      .request(server)
      .get("/api/product/category/4")
      .end((err, res) => {
        if (err) done(err);
        res.should.have.status(200);
        res.body.should.be.jsonSchema(productFromCategorySchema);

        res.body.id.should.be.eql(4);
        res.body.totalItems.should.be.eql(12);
        res.body.products.length.should.be.eql(10);
        done();
      });
  });
});
