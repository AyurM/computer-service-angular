process.env.NODE_ENV = "test";

const fs = require("fs");
const path = require("path");
const chai = require("chai");
const server = require("../../server");
const should = chai.should();

chai.use(require("chai-http"));
chai.use(require("chai-json-schema"));

//Load JSON-schemas
const categorySchema = JSON.parse(
  fs.readFileSync(path.join(__dirname, "../../schemas/category.json"))
);
const categoriesSchema = JSON.parse(
  fs.readFileSync(path.join(__dirname, "../../schemas/categories.json"))
);
const filterValuesSchema = JSON.parse(
  fs.readFileSync(path.join(__dirname, "../../schemas/filter-values.json"))
);

//Add schemas to validator to use $ref correctly
chai.tv4.addSchema("/schemas/category.json", categorySchema);
chai.tv4.addSchema("/schemas/categories.json", categoriesSchema);
chai.tv4.addSchema("/schemas/filter-values.json", filterValuesSchema);

describe("GET /categories", () => {
  it("should get all product categories", (done) => {
    chai
      .request(server)
      .get("/api/shop/categories")
      .end((err, res) => {
        if (err) done(err);
        res.should.have.status(200);
        res.body.should.be.jsonSchema(categoriesSchema);

        res.body.length.should.be.eql(6);
        done();
      });
  });
});

describe("GET /category/:id", () => {
  it("should get 2nd product category", (done) => {
    chai
      .request(server)
      .get("/api/shop/category/2")
      .end((err, res) => {
        if (err) done(err);
        res.should.have.status(200);
        res.body.should.be.jsonSchema(categorySchema);

        res.body.id.should.be.eql(2);
        res.body.name.should.be.eql("Пульты ДУ");
        done();
      });
  });
});

describe("GET /category/:id", () => {
  it("should get status 404 when categoryId is wrong", (done) => {
    chai
      .request(server)
      .get("/api/shop/category/9")
      .end((err, res) => {
        if (err) done(err);
        res.should.have.status(404);
        done();
      });
  });
});

describe("GET /subcategories/:parentId", () => {
  it("should get subcategories of specific category", (done) => {
    chai
      .request(server)
      .get("/api/shop/subcategories/3")
      .end((err, res) => {
        if (err) done(err);
        res.should.have.status(200);
        res.body.should.be.jsonSchema(categoriesSchema);

        res.body.length.should.be.eql(3);
        done();
      });
  });
});

describe("GET /subcategories/:parentId", () => {
  it("should get empty array if subcategories don't exist", (done) => {
    chai
      .request(server)
      .get("/api/shop/subcategories/1")
      .end((err, res) => {
        if (err) done(err);
        res.should.have.status(200);
        res.body.should.be.a("array");
        res.body.length.should.be.eql(0);
        done();
      });
  });
});

describe("GET /subcategories/:parentId", () => {
  it("should get status 404 if parent category doesn't exist", (done) => {
    chai
      .request(server)
      .get("/api/shop/subcategories/9")
      .end((err, res) => {
        if (err) done(err);
        res.should.have.status(404);
        done();
      });
  });
});

describe("GET /filter_values/:categoryId", () => {
  it(`should get list of manufacturers and product attribute
  values for category with item count`, (done) => {
    chai
      .request(server)
      .get("/api/shop/filter_values/4")
      .end((err, res) => {
        if (err) done(err);
        res.should.have.status(200);
        res.body.should.be.jsonSchema(filterValuesSchema);

        res.body.id.should.be.eql(4);
        res.body.manufacturers.length.should.be.eql(4);
        res.body.attrGroups.length.should.be.eql(2);
        done();
      });
  });
});

describe("GET /filter_values/:categoryId", () => {
  it("should get status 404 when categoryId is wrong", (done) => {
    chai
      .request(server)
      .get("/api/shop/filter_values/9")
      .end((err, res) => {
        if (err) done(err);
        res.should.have.status(404);
        done();
      });
  });
});
