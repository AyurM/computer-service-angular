if (process.env.NODE_ENV === "test") {
  require("dotenv").config({ path: "./backend/.env.test" });
} else {
  require("dotenv").config({ path: "./backend/.env" });
}

const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");

const shopRoutes = require("./routes/shop");
const productRoutes = require("./routes/product");
const priceListRoutes = require("./routes/price-list");
const ErrorController = require("./controllers/error-controller");

const app = express();

app.use(bodyParser.json());
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});

app.use("/api/shop", shopRoutes);
app.use("/api/product", productRoutes);
app.use("/api/price-list", priceListRoutes);

app.use(ErrorController.error404);

app.use((error, req, res, next) => {
  res
    .status(error.httpStatusCode)
    .json({ status: error.httpStatusCode, errorMessage: error.message });
});

module.exports = app;
