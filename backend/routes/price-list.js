const express = require("express");
const router = express.Router();

const PriceListController = require("../controllers/price-list-controller");

//GET all price-list categories with respective items
router.get("/full", PriceListController.getFullPriceList);

//GET all price-list items from category with id = categoryId
router.get("/category/:categoryId", PriceListController.getPriceListCategory);

module.exports = router;
