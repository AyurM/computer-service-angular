const express = require("express");
const router = express.Router();

const ProductController = require("../controllers/product-controller");

//GET product with id = productId
router.get("/id/:productId", ProductController.getProduct);

//GET all products from category with id = categoryId
router.get("/category/:categoryId", ProductController.getProductsFromCategory);

//Find all products matching filter
router.get("/filter", ProductController.getFilteredProducts);

module.exports = router;
