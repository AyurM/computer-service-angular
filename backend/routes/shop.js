const express = require("express");
const router = express.Router();

const ShopController = require("../controllers/shop-controller");

//GET all categories
router.get("/categories", ShopController.getCategories);

//GET category with id = categoryId
router.get("/category/:categoryId", ShopController.getCategory);

//GET subcategories of category with id = parentId
router.get("/subcategories/:parentId", ShopController.getSubcategories);

//GET all product attributes and their values for category with id = categoryId,
//also includes manufacturers
router.get("/filter_values/:categoryId", ShopController.getFilterValues);

module.exports = router;
