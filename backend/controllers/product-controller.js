const ProductQueries = require("../db/queries/product-queries");
const ErrorController = require("./error-controller");
const QueryUtils = require("../db/queries/query-utils");

//Query specific category with its products and their manufacturer's name
exports.getProductsFromCategory = async (req, res, next) => {
  try {
    // await new Promise((resolve) => setTimeout(resolve, 3000));

    const page = +req.query.page || 1;
    const itemsPerPage = +req.query.itemsperpage || 10;

    const category = await ProductQueries.queryCategoryWithProductsCountAndPriceRange(
      req.params.categoryId,
      true,
      true
    );

    const products = await ProductQueries.queryProductsWithManufNameAndAttributesFromCategory(
      req.params.categoryId,
      page,
      itemsPerPage
    );

    const result = QueryUtils.mergeProductsFromCategoryQueryResults(
      products,
      category,
      page
    );

    res.status(200).json(result);
  } catch (err) {
    return ErrorController.handleError(next, err);
  }
};

//Query specific product, its manufacturer's name and its attributes
exports.getProduct = async (req, res, next) => {
  try {
    const product = await ProductQueries.queryProductWithManufNameAndAttributesById(
      req.params.productId
    );
    if (!product) {
      return ErrorController.handleError(next, null, 404, "Товар не найден");
    }
    product.dataValues.productCategories =
      product.dataValues.productCategories[0];

    //Manually rename result object property
    //(from "count" to "totalItems")
    delete Object.assign(product.dataValues, {
      category: product.dataValues.productCategories,
    }).productCategories;

    res.status(200).json(product);
  } catch (err) {
    return ErrorController.handleError(next, err);
  }
};

exports.getFilteredProducts = async (req, res, next) => {
  try {
    const categoryId = +req.query.category;
    const page = +req.query.page || 1;

    const filteredProducts = await ProductQueries.queryFilteredProducts(
      req.query
    );

    const attrs = await ProductQueries.queryAttributesForProducts(
      filteredProducts.rows
    );

    const category = await ProductQueries.queryCategoryWithProductsCountAndPriceRange(
      categoryId,
      false,
      true
    );

    const result = QueryUtils.mergeProductsFilteringQueryResults(
      filteredProducts,
      attrs,
      category,
      page
    );

    res.status(200).json(result);
  } catch (err) {
    return ErrorController.handleError(next, err);
  }
};
