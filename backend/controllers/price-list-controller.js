const PriceListQueries = require("../db/queries/price-list-queries");
const ErrorController = require("./error-controller");

//Query all price list categories and their respective items
exports.getFullPriceList = async (req, res, next) => {
  try {
    const priceListCategories = await PriceListQueries.queryFullPriceList();
    res.status(200).json(priceListCategories);
  } catch (err) {
    return ErrorController.handleError(next, err);
  }
};

//Query specific price list category and its items
exports.getPriceListCategory = async (req, res, next) => {
  try {
    const priceListCategory = await PriceListQueries.queryPriceListCategory(
      req.params.categoryId
    );
    if (!priceListCategory) {
      return ErrorController.handleError(
        next,
        null,
        404,
        "Категория не найдена"
      );
    }
    res.status(200).json(priceListCategory);
  } catch (err) {
    return ErrorController.handleError(next, err);
  }
};
