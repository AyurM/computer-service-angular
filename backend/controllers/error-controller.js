exports.handleError = (
  next,
  err,
  statusCode = 500,
  errorMessage = "Ошибка на сервере. Попробуйте повторить запрос позже."
) => {
  if (err) {
    console.log(err);
  }
  const error = new Error();
  error.httpStatusCode = statusCode;
  error.message = errorMessage;
  return next(error);
};

exports.error404 = (req, res, next) => {
  const error = new Error();
  error.httpStatusCode = 404;
  error.message = "Запрашиваемый ресурс не найден";
  return next(error);
};
