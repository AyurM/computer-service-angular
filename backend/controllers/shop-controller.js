const ShopQueries = require("../db/queries/shop-queries");
const ErrorController = require("./error-controller");
const QueryUtils = require("../db/queries/query-utils");

//Query all categories and count associated products
exports.getCategories = async (req, res, next) => {
  try {
    const categories = await ShopQueries.queryAllCategoriesWithProductsCount();
    res.status(200).json(categories);
  } catch (err) {
    return ErrorController.handleError(next, err);
  }
};

//Query specific category and count associated products
exports.getCategory = async (req, res, next) => {
  try {
    const category = await ShopQueries.queryCategoryWithProductsCount(
      req.params.categoryId
    );

    if (!category.dataValues.id) {
      return ErrorController.handleError(
        next,
        null,
        404,
        "Категория не найдена"
      );
    }

    res.status(200).json(category);
  } catch (err) {
    return ErrorController.handleError(next, err);
  }
};

//Query children of specific category and count their associated products
exports.getSubcategories = async (req, res, next) => {
  try {
    const categories = await ShopQueries.querySubcategoriesWithProductsCount(
      req.params.parentId
    );
    res.status(200).json(categories);
  } catch (err) {
    return ErrorController.handleError(next, err);
  }
};

//Query product attributes and their values for specific category,
//also includes product manufacturers
exports.getFilterValues = async (req, res, next) => {
  try {
    const attrValues = await ShopQueries.queryProductAttributesWithAllValuesForCategory(
      req.params.categoryId
    );

    const fetchedManufacturers = await ShopQueries.queryManufacturersForCategory(
      req.params.categoryId
    );

    if (!fetchedManufacturers) {
      return ErrorController.handleError(
        next,
        null,
        404,
        "Категория не найдена"
      );
    }

    const result = QueryUtils.mergeFilterValuesQueryResults(
      attrValues,
      fetchedManufacturers
    );
    res.status(200).json(result);
  } catch (err) {
    return ErrorController.handleError(next, err);
  }
};
