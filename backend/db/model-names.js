module.exports = {
  PRODUCT: "product",
  PRODUCT_CATEGORY: "productCategory",
  PRODUCT_TO_CATEGORY: "productToCategory",
  MANUFACTURER: "manufacturer",
  ATTRIBUTE_GROUP: "attributeGroup",
  ATTRIBUTE: "attribute",
  ATTRIBUTE_TO_PRODUCT: "attributeToProduct",
  PRICE_LIST_CATEGORY: "priceListCategory",
  PRICE_LIST_ITEM: "priceListItem",
};
