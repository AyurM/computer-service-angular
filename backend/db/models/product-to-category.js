const {DataTypes} = require('sequelize');

const ProductToCategory = {
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    }
};

module.exports = ProductToCategory;