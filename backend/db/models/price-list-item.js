const { DataTypes } = require("sequelize");

const PriceListItem = {
  id: {
    type: DataTypes.INTEGER.UNSIGNED,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  price: {
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
  },
  isPriceFrom: {
    type: DataTypes.BOOLEAN,
    defaultValue: false,
  },
};

module.exports = PriceListItem;
