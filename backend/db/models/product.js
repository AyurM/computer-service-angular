const { DataTypes } = require("sequelize");

const Product = {
  id: {
    type: DataTypes.INTEGER.UNSIGNED,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  imageUrl: {
    type: DataTypes.STRING,
    allowNull: false,
    defaultValue: "",
  },
  price: {
    type: DataTypes.INTEGER.UNSIGNED,
    allowNull: false,
  },
  discountPrice: {
    type: DataTypes.INTEGER.UNSIGNED,
    defaultValue: 0,
  },
  available: {
    type: DataTypes.INTEGER,
    defaultValue: 0,
  },
  description: {
    type: DataTypes.STRING,
    allowNull: false,
    defaultValue: "",
  },
};

module.exports = Product;
