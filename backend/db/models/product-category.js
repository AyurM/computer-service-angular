const {DataTypes} = require('sequelize');

const ProductCategory = {
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    imageUrl: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: ""
    },
    parentId: {
        type: DataTypes.INTEGER.UNSIGNED,
        defaultValue: 0
    }
};

module.exports = ProductCategory;