const { DataTypes } = require("sequelize");

const AttrToProduct = {
  id: {
    type: DataTypes.INTEGER.UNSIGNED,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
  },
  value: {
    type: DataTypes.STRING,
    allowNull: false,
  },
};

module.exports = AttrToProduct;
