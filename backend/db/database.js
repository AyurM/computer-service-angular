if (process.env.NODE_ENV === "test") {
  require("dotenv").config({ path: "../.env.test" });
} else {
  require("dotenv").config({ path: "../.env" });
}

const Sequelize = require("sequelize");

const ModelNames = require("./model-names");
const Product = require("./models/product");
const Manufacturer = require("./models/manufacturer");
const ProductCategory = require("./models/product-category");
const ProductToCategory = require("./models/product-to-category");

const PriceListCategory = require("./models/price-list-category");
const PriceListItem = require("./models/price-list-item");

const AttributeGroup = require("./models/attribute-group");
const Attribute = require("./models/attribute");
const AttrToProduct = require("./models/attribute-to-product");

const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_PASSWORD,
  {
    dialect: "mysql",
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    logging: false,
  }
);

//Product categories
const ProductCategoryModel = sequelize.define(
  ModelNames.PRODUCT_CATEGORY,
  ProductCategory
);
const ProductModel = sequelize.define(ModelNames.PRODUCT, Product);
const ProductToCategoryModel = sequelize.define(
  ModelNames.PRODUCT_TO_CATEGORY,
  ProductToCategory,
  { timestamps: false }
);
ProductModel.belongsToMany(ProductCategoryModel, {
  through: ProductToCategoryModel,
});
ProductCategoryModel.belongsToMany(ProductModel, {
  through: ProductToCategoryModel,
});

// ProductModel.belongsTo(ProductCategoryModel);
// ProductCategoryModel.hasMany(ProductModel);

//Manufacturer
const ManufacturerModel = sequelize.define(
  ModelNames.MANUFACTURER,
  Manufacturer,
  {
    timestamps: false,
  }
);
ManufacturerModel.hasMany(ProductModel);
ProductModel.belongsTo(ManufacturerModel);

//Product Attributes
const AttrGroupModel = sequelize.define(
  ModelNames.ATTRIBUTE_GROUP,
  AttributeGroup,
  {
    timestamps: false,
  }
);
const AttributeModel = sequelize.define(ModelNames.ATTRIBUTE, Attribute, {
  timestamps: false,
});
const AttrToProductModel = sequelize.define(
  ModelNames.ATTRIBUTE_TO_PRODUCT,
  AttrToProduct,
  { timestamps: false }
);
AttrGroupModel.hasMany(AttributeModel);
AttributeModel.belongsTo(AttrGroupModel);
ProductCategoryModel.hasMany(AttrGroupModel);
AttrGroupModel.belongsTo(ProductCategoryModel);
ProductModel.belongsToMany(AttributeModel, { through: AttrToProductModel });
AttributeModel.belongsToMany(ProductModel, { through: AttrToProductModel });

//Price-List
const PriceListCategoryModel = sequelize.define(
  ModelNames.PRICE_LIST_CATEGORY,
  PriceListCategory
);
const PriceListItemModel = sequelize.define(
  ModelNames.PRICE_LIST_ITEM,
  PriceListItem
);
PriceListCategoryModel.hasMany(PriceListItemModel);
PriceListItemModel.belongsTo(PriceListCategoryModel);

module.exports = {
  sequelize,
  ProductCategoryModel,
  ProductModel,
  ManufacturerModel,
  AttrGroupModel,
  AttributeModel,
  AttrToProductModel,
  PriceListCategoryModel,
  PriceListItemModel,
};
