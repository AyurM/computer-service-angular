const Db = require("../database");
const { Sequelize, Op } = require("sequelize");

exports.mergeProductsFromCategoryQueryResults = (
  products,
  categoryInfo,
  page
) => {
  products.forEach((product) => {
    flattenManufacturer(product);
    addCategoryInfo(product, categoryInfo);
  });
  return { ...categoryInfo.dataValues, page: page, products: products };
};

exports.mergeProductsFilteringQueryResults = (
  filteredProducts,
  productAttrs,
  categoryInfo,
  page
) => {
  filteredProducts.rows.forEach((product) => {
    appendAttributes(product, productAttrs);
    flattenManufacturer(product);
    addCategoryInfo(product, categoryInfo);
  });

  filteredProducts.page = page;
  filteredProducts.count = filteredProducts.count.length;

  //Manually rename result object property
  //(from "count" to "totalItems")
  delete Object.assign(filteredProducts, {
    totalItems: filteredProducts.count,
  }).count;

  //Manually rename result object property
  // (from "rows" to "products")
  delete Object.assign(filteredProducts, {
    products: filteredProducts.rows,
  }).rows;

  return { ...categoryInfo.dataValues, ...filteredProducts };
};

exports.mergeFilterValuesQueryResults = (attrValues, fetchedManufacturers) => {
  attrValues.forEach((attrGroup) => {
    extractAttributesValuesAndItemsCount(attrGroup);
  });

  //Manually rename result object property
  //(from "products" to "manufacturers")
  delete Object.assign(fetchedManufacturers.dataValues, {
    manufacturers: fetchedManufacturers.dataValues.products,
  }).products;

  return { ...fetchedManufacturers.dataValues, attrGroups: attrValues };
};

exports.getAttributesFieldForCategoryQuery = (
  includeItemCount,
  includePriceRange
) => {
  const result = ["id", "name"];
  if (includeItemCount) {
    result.push([
      Sequelize.fn("COUNT", Sequelize.col(`${Db.ProductModel.tableName}.id`)),
      "totalItems",
    ]);
  }
  if (includePriceRange) {
    result.push([
      Sequelize.fn("MIN", Sequelize.col(`${Db.ProductModel.tableName}.price`)),
      "minPrice",
    ]);
    result.push([
      Sequelize.fn("MAX", Sequelize.col(`${Db.ProductModel.tableName}.price`)),
      "maxPrice",
    ]);
  }
  return result;
};

exports.getManufacturerFilterValues = (manufacturerParam) => {
  if (!manufacturerParam) {
    return [];
  }
  return manufacturerParam.split(",");
};

exports.generateQueryFilter = (queryParams) => {
  const attributeFilter = getAttributeFilterCondition(queryParams);
  const priceFilter = getPriceFilterCondition(queryParams);
  const nameFilter = getNameFilterCondition(queryParams);

  return { [Op.and]: [priceFilter, attributeFilter, nameFilter] };
};

exports.getSortByOption = (queryParams) => {
  if (!queryParams.sortby) {
    return "`product`.`price` ASC";
  }
  switch (queryParams.sortby) {
    case "PRICE_ASC":
      return "`product`.`price` ASC";
    case "PRICE_DESC":
      return "`product`.`price` DESC";
    case "NAME_ASC":
      return "`product`.`name` ASC";
    default:
      return "`product`.`price` ASC";
  }
};

getAttributeFilterCondition = (queryParams) => {
  if (!queryParams) {
    return {};
  }

  const filterData = [];

  Object.keys(queryParams).forEach((key) => {
    if (key.startsWith("attr")) {
      filterData.push({
        attributeId: key.substring(4),
        values: queryParams[key].split(","),
      });
    }
  });

  if (filterData.length === 0) {
    return {};
  }

  if (filterData.length === 1) {
    return processFirstFilterAttribute(filterData[0]);
  } else {
    return processMultipleFilterAttributes(filterData);
  }
};

processMultipleFilterAttributes = (filterData) => {
  condition = [];
  filterData.forEach((data, index) => {
    if (index === 0) {
      condition.push(processFirstFilterAttribute(data));
    } else {
      condition.push(
        getSequelizeLiteralForFilterAttribute(data.attributeId, data.values)
      );
    }
  });
  return { [Op.and]: condition };
};

processFirstFilterAttribute = (filterData) => {
  if (filterData.values.length === 1) {
    return proccesFilterValue(filterData.attributeId, filterData.values[0]);
  } else {
    let condition = [];
    filterData.values.forEach((value) => {
      condition.push(proccesFilterValue(filterData.attributeId, value));
    });
    return { [Op.or]: condition };
  }
};

proccesFilterValue = (attributeId, value) => {
  const keyKey = `$${Db.AttributeModel.tableName}->${Db.AttrToProductModel.name}.attributeId$`;
  const valueKey = `$${Db.AttributeModel.tableName}->${Db.AttrToProductModel.name}.value$`;
  return { [Op.and]: [{ [keyKey]: attributeId }, { [valueKey]: value }] };
};

getSequelizeLiteralForFilterAttribute = (attributeId, values) => {
  return Sequelize.literal(
    "`" +
      `${Db.ProductModel.name}` +
      "`.`id` IN (SELECT `" +
      `${Db.ProductModel.name}` +
      "`.`id` FROM `" +
      `${Db.ProductModel.tableName}` +
      "` AS `" +
      `${Db.ProductModel.name}` +
      "` LEFT OUTER JOIN ( `" +
      `${Db.AttrToProductModel.tableName}` +
      "` AS `" +
      `${Db.AttributeModel.tableName}` +
      "->" +
      `${Db.AttrToProductModel.name}` +
      "` INNER JOIN `" +
      `${Db.AttributeModel.tableName}` +
      "` ON `" +
      `${Db.AttributeModel.tableName}` +
      "`.`id` = `" +
      `${Db.AttributeModel.tableName}` +
      "->" +
      `${Db.AttrToProductModel.name}` +
      "`.`attributeId`) ON `" +
      `${Db.ProductModel.name}` +
      "`.`id` = `" +
      `${Db.AttributeModel.tableName}` +
      "->" +
      `${Db.AttrToProductModel.name}` +
      "`.`productId` WHERE (" +
      getSequelizeLiteralCondition(attributeId, values) +
      "))"
  );
};

getSequelizeLiteralCondition = (attrId, values) => {
  let result = "";
  values.forEach((value, index) => {
    result +=
      "(`" +
      `${Db.AttributeModel.tableName}` +
      "->" +
      `${Db.AttrToProductModel.name}` +
      "`.`attributeId` = " +
      attrId +
      " AND `" +
      `${Db.AttributeModel.tableName}` +
      "->" +
      `${Db.AttrToProductModel.name}` +
      "`.`value` = '" +
      value +
      "')";
    if (index < values.length - 1) {
      result += " OR ";
    }
  });
  return result;
};

getPriceFilterCondition = (queryParams) => {
  if (!queryParams.minprice && !queryParams.maxprice) {
    return {};
  }
  if (+queryParams.minprice === 0 && +queryParams.maxprice === 0) {
    return {};
  }

  if (
    +queryParams.minprice > 0 &&
    (!queryParams.maxprice || +queryParams.maxprice === 0)
  ) {
    return { price: { [Op.gte]: +queryParams.minprice } };
  }

  if (
    (!queryParams.minprice || +queryParams.minprice === 0) &&
    +queryParams.maxprice > 0
  ) {
    return { price: { [Op.lte]: +queryParams.maxprice } };
  }

  return {
    price: { [Op.gte]: +queryParams.minprice, [Op.lte]: +queryParams.maxprice },
  };
};

getNameFilterCondition = (queryParams) => {
  if (!queryParams.query) {
    return {};
  }

  return { name: { [Op.substring]: queryParams.query } };
};

flattenManufacturer = (product) => {
  product.dataValues.manufacturerName = product.dataValues.manufacturer.name;
  delete product.dataValues.manufacturer;
};

appendAttributes = (product, fetchedAttributes) => {
  product.dataValues.attributes = fetchedAttributes.find((attr) => {
    return attr.dataValues.id === product.dataValues.id;
  }).dataValues.attributes;
};

addCategoryInfo = (product, categoryInfo) => {
  product.dataValues.category = {
    id: categoryInfo.dataValues.id,
    name: categoryInfo.dataValues.name,
  };
};

extractAttributesValuesAndItemsCount = (attributeGroup) => {
  attributeGroup.dataValues.attributes.forEach((attribute) => {
    attribute.dataValues.values = [];
    attribute.dataValues.products.forEach((product) => {
      attribute.dataValues.values.push(product.dataValues);
    });

    delete attribute.dataValues.products;
  });
};

/* Запрос для фильтрации товаров по нескольким атрибутам:
SELECT
    `product`.`id`,
    `product`.`name`,
    `product`.`imageUrl`,
    `product`.`price`,
    `product`.`discountPrice`,
    `product`.`available`,
    `product`.`description`,
    `product`.`createdAt`,
    `product`.`updatedAt`,
    `product`.`manufacturerId`
FROM
    `products` AS `product`
        LEFT OUTER JOIN
    (`attributeToProducts` AS `attributes->attributeToProduct`
    INNER JOIN `attributes` AS `attributes` ON `attributes`.`id` = `attributes->attributeToProduct`.`attributeId`) ON `product`.`id` = `attributes->attributeToProduct`.`productId`
WHERE
    (`attributes->attributeToProduct`.`attributeId` = 1
        AND `attributes->attributeToProduct`.`value` = 'Полевой'
        AND `product`.`id` IN (SELECT
            `product`.`id`
        FROM
            `products` AS `product`
                LEFT OUTER JOIN
            (`attributeToProducts` AS `attributes->attributeToProduct`
            INNER JOIN `attributes` AS `attributes` ON `attributes`.`id` = `attributes->attributeToProduct`.`attributeId`) ON `product`.`id` = `attributes->attributeToProduct`.`productId`
        WHERE
            ((`attributes->attributeToProduct`.`attributeId` = 2
                AND `attributes->attributeToProduct`.`value` = 'N-канал'))));


Методы в данном файле генерируют условия для WHERE для Sequelize.
См. https://sequelize.org/master/manual/model-querying-basics.html#advanced-queries-with-functions--not-just-columns-
                */
