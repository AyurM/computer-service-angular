const Db = require("../database");
const Sequelize = require("sequelize");

exports.queryAllCategoriesWithProductsCount = () => {
  return Db.ProductCategoryModel.findAll({
    attributes: {
      exclude: ["createdAt", "updatedAt"],
      include: [
        [
          Sequelize.fn(
            "COUNT",
            Sequelize.col(`${Db.ProductModel.tableName}.id`)
          ),
          "totalItems",
        ],
      ],
    },
    include: [{ model: Db.ProductModel, attributes: [] }],
    group: [Sequelize.col("id")],
  });
};

exports.queryCategoryWithProductsCount = (categoryId) => {
  return Db.ProductCategoryModel.findOne({
    where: {
      id: categoryId,
    },
    attributes: {
      exclude: ["createdAt", "updatedAt"],
      include: [
        [
          Sequelize.fn(
            "COUNT",
            Sequelize.col(`${Db.ProductModel.tableName}.id`)
          ),
          "totalItems",
        ],
      ],
    },
    include: [{ model: Db.ProductModel, attributes: [] }],
  });
};

exports.querySubcategoriesWithProductsCount = (parentCategoryId) => {
  return Db.ProductCategoryModel.findAll({
    where: {
      parentId: parentCategoryId,
    },
    attributes: {
      exclude: ["createdAt", "updatedAt"],
      include: [
        [
          Sequelize.fn(
            "COUNT",
            Sequelize.col(`${Db.ProductModel.tableName}.id`)
          ),
          "totalItems",
        ],
      ],
    },
    include: [{ model: Db.ProductModel, attributes: [] }],
    group: [Sequelize.col("id")],
  });
};

exports.queryProductAttributesWithAllValuesForCategory = (categoryId) => {
  return Db.AttrGroupModel.findAll({
    where: {
      productCategoryId: categoryId,
    },
    attributes: {
      exclude: [`${Db.ProductCategoryModel.name}Id`],
    },
    include: {
      model: Db.AttributeModel,
      attributes: {
        exclude: [`${Db.AttrGroupModel.name}Id`],
      },
      include: {
        model: Db.ProductModel,
        attributes: [
          [
            Sequelize.literal(
              "`" +
                Db.AttributeModel.tableName +
                "->" +
                Db.ProductModel.tableName +
                "->" +
                Db.AttrToProductModel.name +
                "`.`value`"
            ),
            "value",
          ],
          //Count quantity of items with this value
          [
            Sequelize.fn(
              "COUNT",
              Sequelize.literal(
                "`" +
                  Db.AttributeModel.tableName +
                  "->" +
                  Db.ProductModel.tableName +
                  "->" +
                  Db.AttrToProductModel.name +
                  "`.`value`"
              )
            ),
            "items",
          ],
        ],
        through: {
          attributes: [],
        },
      },
    },
    group: [
      `${Db.AttributeModel.tableName}->${Db.ProductModel.tableName}->${Db.AttrToProductModel.name}.value`,
    ],
  });
};

exports.queryManufacturersForCategory = (categoryId) => {
  return Db.ProductCategoryModel.findOne({
    where: {
      id: categoryId,
    },
    attributes: ["id", "name"],
    include: {
      model: Db.ProductModel,
      attributes: [
        [
          Sequelize.literal(
            "`" +
              Db.ProductModel.tableName +
              "->" +
              Db.ManufacturerModel.name +
              "`.`name`"
          ),
          "value",
        ],
        [
          Sequelize.fn(
            "COUNT",
            Sequelize.literal(
              "`" +
                Db.ProductModel.tableName +
                "->" +
                Db.ManufacturerModel.name +
                "`.`name`"
            )
          ),
          "items",
        ],
      ],
      through: {
        attributes: [],
      },
      include: {
        model: Db.ManufacturerModel,
        attributes: [],
      },
    },
    group: [`${Db.ProductModel.tableName}.${Db.ManufacturerModel.name}.name`],
  });
};
