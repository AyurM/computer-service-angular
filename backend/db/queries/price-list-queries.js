const Db = require("../database");

exports.queryFullPriceList = () => {
  return Db.PriceListCategoryModel.findAll({
    attributes: {
      exclude: ["createdAt", "updatedAt"],
    },
    include: [
      {
        model: Db.PriceListItemModel,
        attributes: {
          exclude: [
            "createdAt",
            "updatedAt",
            `${Db.PriceListCategoryModel.name}Id`,
          ],
        },
      },
    ],
  });
};

exports.queryPriceListCategory = (categoryId) => {
  return Db.PriceListCategoryModel.findOne({
    where: {
      id: categoryId,
    },
    attributes: {
      exclude: ["createdAt", "updatedAt"],
    },
    include: [
      {
        model: Db.PriceListItemModel,
        attributes: {
          exclude: [
            "createdAt",
            "updatedAt",
            `${Db.PriceListCategoryModel.name}Id`,
          ],
        },
      },
    ],
  });
};
