const Db = require("../database");
const { Sequelize, Op } = require("sequelize");
const QueryUtils = require("./query-utils");

exports.queryCategoryWithProductsCountAndPriceRange = (
  categoryId,
  includeItemsCount,
  includePriceRange
) => {
  return Db.ProductCategoryModel.findOne({
    where: {
      id: categoryId,
    },
    attributes: QueryUtils.getAttributesFieldForCategoryQuery(
      includeItemsCount,
      includePriceRange
    ),
    include: [
      {
        model: Db.ProductModel,
        attributes: [],
      },
    ],
  });
};

exports.queryProductsWithManufNameAndAttributesFromCategory = (
  categoryId,
  page,
  itemsPerPage
) => {
  return Db.ProductModel.findAll({
    limit: itemsPerPage,
    offset: (page - 1) * itemsPerPage,
    attributes: {
      exclude: ["createdAt", "updatedAt", `${Db.ManufacturerModel.name}Id`],
    },
    include: [
      {
        model: Db.ProductCategoryModel,
        where: {
          id: categoryId,
        },
        attributes: [],
        through: {
          attributes: [],
        },
      },
      {
        model: Db.ManufacturerModel,
        attributes: ["name"],
      },
      {
        model: Db.AttributeModel,
        attributes: {
          include: [
            "id",
            "name",
            [
              Sequelize.literal(
                "`" +
                  Db.AttributeModel.tableName +
                  "->" +
                  Db.AttrToProductModel.name +
                  "`.`value`"
              ),
              "attrValue",
            ],
            [
              Sequelize.literal(
                "`" +
                  Db.AttributeModel.tableName +
                  "->" +
                  Db.AttrGroupModel.name +
                  "`.`name`"
              ),
              "attrGroupName",
            ],
          ],
        },
        through: {
          attributes: [],
        },
        include: {
          model: Db.AttrGroupModel,
          attributes: [],
        },
      },
    ],
    order: [["price", "ASC"]],
  });
};

exports.queryFilteredProducts = (queryParams) => {
  const page = +queryParams.page || 1;
  const itemsPerPage = +queryParams.itemsperpage || 10;

  const mfFilter = QueryUtils.getManufacturerFilterValues(
    queryParams.manufacturer
  );
  const queryFilter = QueryUtils.generateQueryFilter(queryParams);

  const orderAndPaginationLiteral =
    QueryUtils.getSortByOption(queryParams) +
    " LIMIT " +
    itemsPerPage +
    " OFFSET " +
    (page - 1) * itemsPerPage;

  return Db.ProductModel.findAndCountAll({
    attributes: {
      exclude: ["createdAt", "updatedAt", `${Db.ManufacturerModel.name}Id`],
    },
    where: queryFilter,
    include: [
      {
        model: Db.ManufacturerModel,
        attributes: ["name"],
        where: {
          name: {
            [Op.or]: mfFilter,
          },
        },
      },
      {
        model: Db.AttributeModel,
        attributes: [],
      },
      {
        model: Db.ProductCategoryModel,
        attributes: [],
        where: {
          id: queryParams.category,
        },
      },
    ],
    order: [Sequelize.literal(orderAndPaginationLiteral)],
    group: ["product.id"],
  });
};

exports.queryProductWithManufNameAndAttributesById = (productId) => {
  return Db.ProductModel.findOne({
    where: {
      id: productId,
    },
    attributes: {
      exclude: ["createdAt", "updatedAt", `${Db.ManufacturerModel.name}Id`],
      include: [
        [
          Sequelize.col(`${Db.ManufacturerModel.name}.name`),
          "manufacturerName",
        ],
      ],
    },
    include: [
      {
        model: Db.ProductCategoryModel,
        attributes: ["id", "name"],
        through: {
          attributes: [],
        },
      },
      {
        model: Db.ManufacturerModel,
        attributes: [],
      },
      {
        model: Db.AttributeModel,
        attributes: {
          include: [
            "id",
            "name",
            [
              Sequelize.literal(
                "`" +
                  Db.AttributeModel.tableName +
                  "->" +
                  Db.AttrToProductModel.name +
                  "`.`value`"
              ),
              "attrValue",
            ],
            [
              Sequelize.literal(
                "`" +
                  Db.AttributeModel.tableName +
                  "->" +
                  Db.AttrGroupModel.name +
                  "`.`name`"
              ),
              "attrGroupName",
            ],
          ],
        },
        through: {
          attributes: [],
        },
        include: {
          model: Db.AttrGroupModel,
          attributes: [],
        },
      },
    ],
  });
};

exports.queryAttributesForProducts = (products) => {
  const productIds = [];
  products.forEach((product) => {
    productIds.push(product.dataValues.id);
  });

  return Db.ProductModel.findAll({
    where: {
      id: productIds,
    },
    attributes: ["id"],
    include: [
      {
        model: Db.AttributeModel,
        attributes: {
          include: [
            "id",
            "name",
            [
              Sequelize.literal(
                "`" +
                  Db.AttributeModel.tableName +
                  "->" +
                  Db.AttrToProductModel.name +
                  "`.`value`"
              ),
              "attrValue",
            ],
            [
              Sequelize.literal(
                "`" +
                  Db.AttributeModel.tableName +
                  "->" +
                  Db.AttrGroupModel.name +
                  "`.`name`"
              ),
              "attrGroupName",
            ],
          ],
        },
        through: {
          attributes: [],
        },
        include: {
          model: Db.AttrGroupModel,
          attributes: [],
        },
      },
    ],
  });
};
