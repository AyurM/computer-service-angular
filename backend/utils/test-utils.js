exports.getParamsFromFilterCriteria = (filterCriteria) => {
  result = { category: filterCriteria.categoryId.toString() };
  result.page = filterCriteria.page.toString();
  result.itemsperpage = filterCriteria.itemsperpage.toString();

  if (filterCriteria.priceRange) {
    if (filterCriteria.priceRange.minPrice > 0) {
      result.minprice = filterCriteria.priceRange.minPrice.toString();
    }
    if (filterCriteria.priceRange.maxPrice > 0) {
      result.maxprice = filterCriteria.priceRange.maxPrice.toString();
    }
  }

  filterCriteria.filterData.forEach((data) => {
    const queryParam = getAttributeQueryParam(data);
    result[queryParam.key] = queryParam.value;
  });

  if (filterCriteria.searchQuery) {
    result.query = filterCriteria.searchQuery;
  }
  return result;
};

getAttributeQueryParam = (filterAttr) => {
  const resultKey =
    filterAttr.attributeId === 0
      ? "manufacturer"
      : `attr${filterAttr.attributeId}`;
  const resultValue = filterAttr.values.join(",");
  return { key: resultKey, value: resultValue };
};
