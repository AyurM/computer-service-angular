import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ComputerRepairPageComponent } from './computer-repair-page/computer-repair-page.component';
import { RepairHeroSectionComponent } from './repair-hero-section/repair-hero-section.component';
import { RepairStepsComponent } from './repair-steps/repair-steps.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    ComputerRepairPageComponent,
    RepairHeroSectionComponent,
    RepairStepsComponent,
  ],
  imports: [
    RouterModule.forChild([
      { path: '', component: ComputerRepairPageComponent },
    ]),
    CommonModule,
    SharedModule,
  ],
})
export class ComputerRepairPageModule {}
