import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { BreadcrumbService } from '../../services/breadcrumb.service';
import { PriceListItem } from '../../shared/price-list/price-list-item.model';

@Component({
  selector: 'app-computer-repair-page',
  templateUrl: './computer-repair-page.component.html',
  styleUrls: ['./computer-repair-page.component.scss'],
})
export class ComputerRepairPageComponent implements OnInit {
  priceListCategory1: string = 'Ремонт ноутбуков';
  priceListCategory2: string = 'Ремонт компьютеров';
  priceList1: PriceListItem[] = [
    {
      title: 'Диагностика ноутбука',
      price: 300,
      isPriceFrom: false,
    },
    {
      title: 'Установка драйверов оборудования',
      price: 300,
      isPriceFrom: true,
    },
    {
      title: 'Установка и настройка ПО',
      price: 500,
      isPriceFrom: true,
    },
    {
      title: 'Лечение от вирусов и вредоносного ПО',
      price: 500,
      isPriceFrom: false,
    },
    {
      title: 'Замена ЖК-дисплея',
      price: 1000,
      isPriceFrom: false,
    },
  ];

  priceList2: PriceListItem[] = [
    {
      title: 'Диагностика системного блока',
      price: 300,
      isPriceFrom: true,
    },
    {
      title: 'Диагностика материнской платы',
      price: 300,
      isPriceFrom: true,
    },
    {
      title: 'Диагностика процессора',
      price: 300,
      isPriceFrom: true,
    },
    {
      title: 'Диагностика жесткого диска',
      price: 300,
      isPriceFrom: true,
    },
    {
      title: 'Устранение неполадок в операционной системе',
      price: 500,
      isPriceFrom: true,
    },
    {
      title: 'Замена компонентов системного блока',
      price: 1000,
      isPriceFrom: false,
    },
  ];

  constructor(
    private breadcrumbService: BreadcrumbService,
    private pageTitle: Title
  ) {}

  ngOnInit(): void {
    this.pageTitle.setTitle('Ремонт компьютеров и ноутбуков в Улан-Удэ');
    this.breadcrumbService.breadcrumbs = [
      { label: 'Главная', url: '' },
      { label: 'Ремонт компьютерной техники', url: '/remont-komputerov' },
    ];
  }
}
