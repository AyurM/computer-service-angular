import { Component, OnInit } from '@angular/core';
import { environment } from '../../environments/environment';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  companyName: string = environment.companyName;

  constructor(private cartService: CartService) {}

  get cartSize(): number {
    return this.cartService.size;
  }

  ngOnInit(): void {}
}
