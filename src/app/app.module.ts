import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { StartSectionComponent } from './start-page/start-section/start-section.component';
import { FeaturesComponent } from './start-page/features/features.component';
import { MainInfoSectionComponent } from './start-page/main-info-section/main-info-section.component';
import { StartPageComponent } from './start-page/start-page/start-page.component';
import { SharedModule } from './shared/shared.module';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ErrorPageComponent,
    StartSectionComponent,
    FeaturesComponent,
    MainInfoSectionComponent,
    StartPageComponent,
    BreadcrumbsComponent,
  ],
  imports: [BrowserModule, HttpClientModule, SharedModule, AppRoutingModule],
  providers: [Title],
  bootstrap: [AppComponent],
})
export class AppModule {}
