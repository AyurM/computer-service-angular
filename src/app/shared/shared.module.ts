import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { PriceListComponent } from './price-list/price-list.component';
import { LoadingIndicatorComponent } from './loading-indicator/loading-indicator.component';
import { CartPageComponent } from './cart/cart-page/cart-page.component';
import { CartItemComponent } from './cart/cart-item/cart-item.component';

@NgModule({
  declarations: [
    PriceListComponent,
    LoadingIndicatorComponent,
    CartPageComponent,
    CartItemComponent,
  ],
  imports: [CommonModule, FormsModule, RouterModule],
  exports: [
    PriceListComponent,
    LoadingIndicatorComponent,
    FormsModule,
    RouterModule,
  ],
})
export class SharedModule {}
