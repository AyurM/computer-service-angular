import { Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

import { CartService } from '../../../services/cart.service';
import { BreadcrumbService } from '../../../services/breadcrumb.service';
import { Product } from '../../../shop/product.model';

@Component({
  selector: 'app-cart-page',
  templateUrl: './cart-page.component.html',
  styleUrls: ['./cart-page.component.scss'],
})
export class CartPageComponent implements OnInit, OnDestroy {
  products: Map<Product, number> = new Map<Product, number>();
  productKeys: Product[] = [];

  private sub: Subscription;

  get totalPrice(): number {
    let result = 0;
    this.products.forEach((quantity, product) => {
      result +=
        (product.discountPrice > 0 ? product.discountPrice : product.price) *
        quantity;
    });

    return result;
  }

  constructor(
    private cartService: CartService,
    private breadcrumbService: BreadcrumbService,
    private pageTitle: Title
  ) {}

  ngOnInit(): void {
    this.pageTitle.setTitle('Ваша корзина');
    this.sub = this.cartService.productsSubject.subscribe((products) => {
      this.products = products;
      this.productKeys = Array.from(this.products.keys());
    });
    this.breadcrumbService.breadcrumbs = [
      { label: 'Главная', url: '' },
      { label: 'Магазин', url: '/shop' },
      {
        label: 'Корзина',
        url: '/shop/cart',
      },
    ];
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
