import { Component, Input, OnInit } from '@angular/core';

import { Product } from '../../../shop/product.model';
import { CartService } from '../../../services/cart.service';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss'],
})
export class CartItemComponent implements OnInit {
  @Input() product: Product;
  @Input() quantity: number;
  isLinkHovered = false;

  constructor(private cartService: CartService) {}

  get totalPrice(): number {
    return (
      (this.product.discountPrice > 0
        ? this.product.discountPrice
        : this.product.price) * this.quantity
    );
  }

  increaseQuantity() {
    this.cartService.addProduct(this.product, 1);
  }

  decreaseQuantity() {
    this.cartService.addProduct(this.product, -1);
  }

  onQuantityInput() {
    if (!this.quantity || this.quantity == null) {
      this.cartService.addProduct(this.product, 1);
      return;
    }

    if (this.quantity > this.product.available) {
      this.cartService.addProduct(this.product, this.product.available);
      return;
    }

    if (this.quantity < 1) {
      this.quantity = 1;
      this.cartService.addProduct(this.product, 1);
    }
  }

  onDeleteProduct() {
    this.cartService.removeProduct(this.product);
  }

  onProductLinkMouseEnter() {
    this.isLinkHovered = true;
  }

  onProductLinkMouseLeave() {
    this.isLinkHovered = false;
  }

  ngOnInit(): void {}
}
