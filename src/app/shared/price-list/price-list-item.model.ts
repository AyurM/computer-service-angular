export interface PriceListItem {
  title: string;
  price: number;
  isPriceFrom: boolean;
}
