import { Component, OnInit, Input } from '@angular/core';
import { PriceListItem } from './price-list-item.model';

@Component({
  selector: 'app-price-list',
  templateUrl: './price-list.component.html',
  styleUrls: ['./price-list.component.scss'],
})
export class PriceListComponent implements OnInit {
  @Input() priceListCategory: string;
  @Input() priceList: PriceListItem[];

  constructor() {}

  ngOnInit(): void {}
}
