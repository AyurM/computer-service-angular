import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

import { ShopService } from '../../services/shop.service';
import { BreadcrumbService } from '../../services/breadcrumb.service';
import { ProductCompareService } from '../../services/product-compare.service';
import { ShopCategory } from '../shop-category.model';
import { Utils } from '../../services/utils/utils';

@Component({
  selector: 'app-shop-products',
  templateUrl: './shop-products.component.html',
  styleUrls: ['./shop-products.component.scss'],
})
export class ShopProductsComponent implements OnInit, OnDestroy {
  category: ShopCategory;
  isLoading: boolean = true;
  showComparePanel = true;

  private serviceSubscriptions: Subscription[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private pageTitle: Title,
    private shopService: ShopService,
    private compareService: ProductCompareService,
    private breadcrumbService: BreadcrumbService
  ) {}

  ngOnInit(): void {
    this.serviceSubscriptions.push(
      this.shopService.categoryDetailsSubject.subscribe((category) => {
        this.category = category;
        this.pageTitle.setTitle(category.name);
        this.breadcrumbService.breadcrumbs = [
          { label: 'Главная', url: '' },
          { label: 'Магазин', url: '/shop' },
          {
            label: this.category.name,
            url: `/shop/category/${this.category.id}`,
          },
        ];
      })
    );

    this.route.params.subscribe((params: Params) => {
      this.shopService.requestCategory(params['id']);
      this.loadProducts(params['id']);
    });

    this.serviceSubscriptions.push(
      this.shopService.errorSubject.subscribe((error) => {
        console.log(error);
        this.router.navigate(['/error'], {
          state: { status: error.status, message: error.message },
        });
      })
    );

    this.serviceSubscriptions.push(
      this.compareService.productsSubject.subscribe((products) => {
        this.showComparePanel = products.size > 0;
      })
    );
  }

  getProductsString(): string {
    return Utils.getProductsPlural(this.category.totalItems);
  }

  private loadProducts(categoryId: string) {
    if (!categoryId) {
      return;
    }
    this.shopService.requestProductsFromCategory(categoryId);
  }

  ngOnDestroy() {
    this.serviceSubscriptions.forEach((sub) => {
      sub.unsubscribe();
    });
  }
}
