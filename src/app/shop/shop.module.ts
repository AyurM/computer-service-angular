import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { ShopPageComponent } from './shop-page/shop-page.component';
import { ShopCategoriesComponent } from './shop-categories/shop-categories.component';
import { ShopProductsComponent } from './shop-products/shop-products.component';
import { ProductListItemComponent } from './product-list-item/product-list-item.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductCardComponent } from './product-card/product-card.component';
import { DropdownDirective } from './dropdown.directive';
import { DropdownSelectorComponent } from './filter/dropdown-selector/dropdown-selector.component';
import { ProductFilterComponent } from './filter/product-filter/product-filter.component';
import { AttributeFilterItemComponent } from './filter/attribute-filter-item/attribute-filter-item.component';
import { PriceFilterItemComponent } from './filter/price-filter-item/price-filter-item.component';
import { AppliedFiltersPanelComponent } from './filter/applied-filters-panel/applied-filters-panel.component';
import { AppliedFilterOptionComponent } from './filter/applied-filter-option/applied-filter-option.component';
import { SearchProductComponent } from './filter/search-product/search-product.component';
import { ComparePanelComponent } from './compare-products/compare-panel/compare-panel.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { SortPanelComponent } from './filter/sort-panel/sort-panel.component';
import { PaginationComponent } from './filter/pagination/pagination.component';
import { ComparePageComponent } from './compare-products/compare-page/compare-page.component';
import { CartPageComponent } from '../shared/cart/cart-page/cart-page.component';

@NgModule({
  declarations: [
    ShopPageComponent,
    ShopCategoriesComponent,
    ShopProductsComponent,
    ProductListItemComponent,
    ProductListComponent,
    ProductFilterComponent,
    AttributeFilterItemComponent,
    PriceFilterItemComponent,
    DropdownDirective,
    DropdownSelectorComponent,
    ProductCardComponent,
    AppliedFiltersPanelComponent,
    AppliedFilterOptionComponent,
    SearchProductComponent,
    ComparePanelComponent,
    ProductDetailsComponent,
    SortPanelComponent,
    PaginationComponent,
    ComparePageComponent,
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: ShopPageComponent,
      },
      { path: 'compare', component: ComparePageComponent },
      { path: 'cart', component: CartPageComponent },
      { path: ':id', component: ShopPageComponent },
      {
        path: 'category/:id',
        component: ShopProductsComponent,
      },
      {
        path: 'details/:id',
        component: ProductDetailsComponent,
      },
    ]),
    CommonModule,
    SharedModule,
  ],
})
export class ShopModule {}
