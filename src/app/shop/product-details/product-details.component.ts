import { Component, OnInit, OnDestroy } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';

import { ShopService } from '../../services/shop.service';
import { CartService } from '../../services/cart.service';
import { BreadcrumbService } from '../../services/breadcrumb.service';
import { Product } from '../product.model';
import { Attribute } from '../../shop/attribute.model';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
})
export class ProductDetailsComponent implements OnInit, OnDestroy {
  productId: string;
  product: Product;
  isLoading = false;
  specs: Map<string, Attribute[]>;
  specGroups: string[];

  sections = ['Описание', 'Характеристики', 'Отзывы'];
  selectedSectionIndex = 1;
  quantity = 1;

  private subs: Subscription[] = [];

  constructor(
    private shopService: ShopService,
    private cartSevice: CartService,
    private breadcrumbService: BreadcrumbService,
    private route: ActivatedRoute,
    private pageTitle: Title
  ) {}

  ngOnInit(): void {
    this.subs.push(
      this.shopService.productDetailsSubject.subscribe((fetchedProduct) => {
        this.product = fetchedProduct;
        this.pageTitle.setTitle(fetchedProduct.name);
        this.specs = this.getSpecsMap(fetchedProduct.attributes);
        this.breadcrumbService.breadcrumbs = [
          { label: 'Главная', url: '' },
          { label: 'Магазин', url: '/shop' },
          {
            label: this.product.category.name,
            url: `/shop/category/${this.product.category.id}`,
          },
          { label: this.product.name, url: `/shop/details/${this.product.id}` },
        ];
      })
    );

    this.subs.push(
      this.shopService.isProductsLoadingSubject.subscribe((loading) => {
        this.isLoading = loading;
      })
    );

    this.route.params.subscribe((params: Params) => {
      this.productId = params['id'];
      this.shopService.requestProduct(this.productId);
    });
  }

  onSectionSelect(index: number) {
    this.selectedSectionIndex = index;
  }

  increaseQuantity() {
    this.quantity++;
  }

  decreaseQuantity() {
    this.quantity--;
  }

  onQuantityInput() {
    if (!this.quantity || this.quantity == null) {
      this.quantity = 1;
      return;
    }

    if (this.quantity > this.product.available) {
      this.quantity = this.product.available;
      return;
    }

    if (this.quantity < 1) {
      this.quantity = 1;
    }
  }

  private getSpecsMap(attrs: Attribute[]): Map<string, Attribute[]> {
    const result = new Map<string, Attribute[]>();
    if (!attrs) {
      return result;
    }

    attrs.forEach((attr) => {
      if (result.has(attr.attrGroupName)) {
        result.get(attr.attrGroupName).push(attr);
      } else {
        result.set(attr.attrGroupName, [attr]);
      }
    });
    this.specGroups = Array.from(result.keys());
    return result;
  }

  onAddToCart() {
    this.cartSevice.addProduct(this.product, this.quantity);
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
