import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { ProductsFromCategory } from '../products-from-category.model';
import { ShopService } from '../../services/shop.service';
import { FilterService } from '../../services/filter.service';
import { FilterAttribute } from '../filter/filter.model';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit, OnDestroy {
  productsInfo: ProductsFromCategory;
  filterAttrs: FilterAttribute[];
  searchQuery: string;
  priceRange: { minPrice: number; maxPrice: number };
  isLoading = true;

  private shopServiceSubs: Subscription[] = [];

  constructor(
    private shopService: ShopService,
    private filterService: FilterService
  ) {}

  get categoryInfo(): string {
    if (!this.productsInfo) {
      return '';
    }

    return JSON.stringify({
      id: this.productsInfo.id,
      name: this.productsInfo.name,
    });
  }

  ngOnInit(): void {
    this.shopServiceSubs.push(
      this.shopService.productsSubject.subscribe((productsInfo) => {
        this.productsInfo = productsInfo;
      })
    );

    this.shopServiceSubs.push(
      this.shopService.isProductsLoadingSubject.subscribe(
        (loadingStatus) => (this.isLoading = loadingStatus)
      )
    );

    this.shopServiceSubs.push(
      this.filterService.filterAttributesSubject.subscribe((attrs) => {
        this.filterAttrs = attrs;
        this.priceRange = { ...this.filterService.priceRange };
      })
    );

    this.shopServiceSubs.push(
      this.filterService.searchQuerySubject.subscribe((query) => {
        this.searchQuery = query;
      })
    );
  }

  getResultIndices(): { start: number; end: number } {
    return this.filterService.productListIndices;
  }

  getPaginationInfo(): { maxPage: number; pages: number[] } {
    return this.filterService.paginationInfo;
  }

  onPageSelect(pageNumber: number) {
    document
      .querySelector('.category-title-container')
      .scrollIntoView({ behavior: 'smooth', block: 'start' });
    this.shopService.changePage(pageNumber);
  }

  ngOnDestroy() {
    this.shopServiceSubs.forEach((sub) => {
      sub.unsubscribe();
    });
  }
}
