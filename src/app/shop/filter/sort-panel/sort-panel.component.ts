import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-sort-panel',
  templateUrl: './sort-panel.component.html',
  styleUrls: ['./sort-panel.component.scss'],
})
export class SortPanelComponent implements OnInit {
  @Input() totalItems: number;
  @Input() productListIndices = { start: 1, end: 1 };

  constructor() {}

  ngOnInit(): void {}
}
