import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { Subscription, Subject } from 'rxjs';

import { ProductsFromCategory } from '../../products-from-category.model';
import { FilterAttribute } from '../filter.model';
import { ShopService } from '../../../services/shop.service';
import { FilterService } from '../../../services/filter.service';

@Component({
  selector: 'app-product-filter',
  templateUrl: './product-filter.component.html',
  styleUrls: ['./product-filter.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ProductFilterComponent implements OnInit, OnDestroy {
  productsInfo: ProductsFromCategory;
  filterAttributes: FilterAttribute[] = [];
  filterAttrsSubject: Subject<FilterAttribute[]>;

  isMobileFilterVisible = false;

  private shopServiceSubs: Subscription[] = [];

  constructor(
    private shopService: ShopService,
    private filterService: FilterService
  ) {}

  ngOnInit(): void {
    this.filterAttrsSubject = this.filterService.filterAttributesSubject;

    this.shopServiceSubs.push(
      this.shopService.productsSubject.subscribe((productInfo) => {
        this.productsInfo = productInfo;
      })
    );

    this.shopServiceSubs.push(
      this.filterService.filterAttributesSubject.subscribe((filterAttrs) => {
        this.filterAttributes = filterAttrs;
      })
    );
  }

  onApplyFilter() {
    this.shopService.applyFilter();
  }

  onResetFilter() {
    this.shopService.resetFilter();
  }

  onDisplayFilterClick() {
    this.isMobileFilterVisible = !this.isMobileFilterVisible;
  }

  ngOnDestroy() {
    this.shopServiceSubs.forEach((sub) => {
      sub.unsubscribe();
    });
  }
}
