import { Component, OnInit, OnDestroy } from '@angular/core';
import { ShopService } from 'src/app/services/shop.service';
import { FilterService } from 'src/app/services/filter.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-search-product',
  templateUrl: './search-product.component.html',
  styleUrls: ['./search-product.component.scss'],
})
export class SearchProductComponent implements OnInit, OnDestroy {
  searchFocused = false;
  searchString = '';

  private searchQuerySub: Subscription;

  constructor(
    private shopService: ShopService,
    private filterService: FilterService
  ) {}

  ngOnInit(): void {
    this.searchQuerySub = this.filterService.searchQuerySubject.subscribe(
      (searchQuery) => {
        this.searchString = searchQuery;
      }
    );
  }

  onSearchFocusIn() {
    this.searchFocused = true;
  }

  onSearchFocusOut() {
    this.searchFocused = false;
  }

  onChange() {
    this.filterService.searchQuery = this.searchString;
  }

  onSearchClick() {
    if (this.searchString !== '') {
      this.shopService.searchProduct();
    }
  }

  ngOnDestroy() {
    this.searchQuerySub.unsubscribe();
  }
}
