import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'app-applied-filter-option',
  templateUrl: './applied-filter-option.component.html',
  styleUrls: ['./applied-filter-option.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppliedFilterOptionComponent implements OnInit {
  @Input() option: { id: number; name: string; value: string };
  @Output() removeOptionEvent = new EventEmitter<{
    id: number;
    name: string;
    value: string;
  }>();

  constructor() {}

  ngOnInit(): void {}

  onClick() {
    this.removeOptionEvent.emit(this.option);
  }
}
