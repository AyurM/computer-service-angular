import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  OnChanges,
} from '@angular/core';

import { FilterAttribute } from '../filter.model';
import { ShopService } from 'src/app/services/shop.service';

@Component({
  selector: 'app-applied-filters-panel',
  templateUrl: './applied-filters-panel.component.html',
  styleUrls: ['./applied-filters-panel.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppliedFiltersPanelComponent implements OnInit, OnChanges {
  @Input() filterAttributes: FilterAttribute[];
  @Input() textSearchString = '';
  @Input() priceRange: { minPrice: number; maxPrice: number };

  appliedFilters: { id: number; name: string; value: string }[] = [];
  appliedFiltersNumber = 0;

  constructor(private shopService: ShopService) {}

  get visible(): boolean {
    return (
      this.appliedFilters.length > 0 ||
      this.textSearchString !== '' ||
      this.priceRange?.minPrice > 0 ||
      this.priceRange?.maxPrice > 0
    );
  }

  get resetAllVisible(): boolean {
    return this.appliedFiltersNumber > 1;
  }

  ngOnChanges() {
    this.getAppliedFilterValues(this.filterAttributes);
    this.appliedFiltersNumber = this.countAppliedFilters();
  }

  ngOnInit(): void {}

  onOptionRemove(removedOption: { id: number; name: string; value: string }) {
    const index = this.appliedFilters.findIndex((el) => {
      return el.id === removedOption.id && el.value === removedOption.value;
    });
    if (index > -1) {
      this.shopService.removeFilterOption(removedOption);
    }
  }

  onResetFilter() {
    this.shopService.resetFilter();
  }

  onResetSearchString() {
    this.textSearchString = '';
    this.shopService.resetSearchQuery();
  }

  onResetMinPrice() {
    this.shopService.resetMinPrice();
  }

  onResetMaxPrice() {
    this.shopService.resetMaxPrice();
  }

  private getAppliedFilterValues(attributes: FilterAttribute[]) {
    this.appliedFilters = [];
    attributes.forEach((attr) => {
      attr.values
        .filter((value) => {
          return value.checked;
        })
        .forEach((appliedValue) => {
          this.appliedFilters.push({
            id: attr.id,
            name: attr.name,
            value: appliedValue.value,
          });
        });
    });
  }

  private countAppliedFilters(): number {
    let result = this.appliedFilters.length;
    if (this.textSearchString !== '') {
      result++;
    }
    if (this.priceRange.minPrice > 0) {
      result++;
    }
    if (this.priceRange.maxPrice > 0) {
      result++;
    }
    return result;
  }
}
