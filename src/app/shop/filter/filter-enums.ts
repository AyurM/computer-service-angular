export enum SortOptions {
  PriceAsc = 'PRICE_ASC',
  PriceDesc = 'PRICE_DESC',
  NameAsc = 'NAME_ASC',
}

export const SortOptionsMap = new Map([
  [SortOptions.PriceAsc, 'по возрастанию цены'],
  [SortOptions.PriceDesc, 'по убыванию цены'],
  [SortOptions.NameAsc, 'по наименованию'],
]);
