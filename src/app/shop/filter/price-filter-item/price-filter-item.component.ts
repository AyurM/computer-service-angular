import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  ViewChild,
  OnDestroy,
  ElementRef,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { ShopService } from '../../../services/shop.service';

import { FilterService } from '../../../services/filter.service';

@Component({
  selector: 'app-price-filter-item',
  templateUrl: './price-filter-item.component.html',
  styleUrls: ['./price-filter-item.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PriceFilterItemComponent implements OnInit, OnDestroy {
  @ViewChild('priceFrom') priceFromInput: ElementRef;
  @ViewChild('priceTo') priceToInput: ElementRef;

  @Input() priceRange: { minPrice: number; maxPrice: number };
  filterPriceRange: { minPrice: number; maxPrice: number } = {
    minPrice: 0,
    maxPrice: 0,
  };
  isOpened = true;

  private priceRangeSub: Subscription;

  constructor(
    private filterService: FilterService,
    private shopService: ShopService
  ) {}

  ngOnInit(): void {
    this.priceRangeSub = this.filterService.priceRangeSubject.subscribe(
      (priceRange) => {
        this.filterPriceRange = priceRange;
        this.priceFromInput.nativeElement.value =
          this.filterPriceRange.minPrice === 0
            ? ''
            : this.filterPriceRange.minPrice;
        this.priceToInput.nativeElement.value =
          this.filterPriceRange.maxPrice === 0
            ? ''
            : this.filterPriceRange.maxPrice;
      }
    );
  }

  onCategoryClick() {
    this.isOpened = !this.isOpened;
  }

  onValueChange(event: any) {
    if (event.target.id === 'price-from') {
      this.filterService.minPrice = +event.target.value;
    } else {
      this.filterService.maxPrice = +event.target.value;
    }
  }

  onPressEnter() {
    this.shopService.searchProduct();
  }

  ngOnDestroy() {
    this.priceRangeSub.unsubscribe();
  }
}
