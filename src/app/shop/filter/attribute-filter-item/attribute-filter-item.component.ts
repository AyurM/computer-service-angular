import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

import { FilterAttribute } from '../filter.model';
import { FilterService } from '../../../services/filter.service';

@Component({
  selector: 'app-attribute-filter-item',
  templateUrl: './attribute-filter-item.component.html',
  styleUrls: ['./attribute-filter-item.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AttributeFilterItemComponent implements OnInit {
  @Input() attribute: FilterAttribute;
  isOpened = false;

  constructor(private filterService: FilterService) {}

  ngOnInit(): void {
    if (this.attribute.id === 0) {
      this.isOpened = true;
    }
  }

  onCategoryClick() {
    this.isOpened = !this.isOpened;
  }

  onValueChange(event: any) {
    this.attribute.values[event.target.id].checked = event.target.checked;
    this.filterService.refreshFilterData(this.attribute);
  }
}
