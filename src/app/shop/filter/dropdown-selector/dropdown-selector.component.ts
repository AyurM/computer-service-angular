import { Component, OnInit } from '@angular/core';
import { ShopService } from '../../../services/shop.service';
import { FilterService } from '../../../services/filter.service';

@Component({
  selector: 'app-dropdown-selector',
  templateUrl: './dropdown-selector.component.html',
  styleUrls: ['./dropdown-selector.component.scss'],
})
export class DropdownSelectorComponent implements OnInit {
  options: string[];
  selectedIndex = 0;

  constructor(
    private shopService: ShopService,
    private filterService: FilterService
  ) {}

  ngOnInit(): void {
    this.options = this.filterService.getSortOptionNames();
    this.selectedIndex = this.filterService.getSelectedSortByOption();
  }

  onItemClick(index: number) {
    this.selectedIndex = index;
    this.shopService.changeSortingOption(this.selectedIndex);
  }
}
