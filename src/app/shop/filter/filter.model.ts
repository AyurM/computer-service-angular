import { SortOptions } from './filter-enums';

export interface FilterValuesResponse {
  id: number;
  name: string;
  manufacturers: { value: string; items: number }[];
  attrGroups: FilterAttributeGroup[];
}

export interface FilterCriteria {
  page: number;
  itemsperpage: number;
  categoryId?: number;
  searchQuery?: string;
  filterData: { attributeId: number; values: string[] }[];
  priceRange?: { minPrice: number; maxPrice: number };
  sortBy?: SortOptions;
}

export interface FilterAttributeGroup {
  id: number;
  name: string;
  attributes: FilterAttribute[];
}

export interface FilterAttribute {
  id: number;
  name: string;
  values: { value: string; items: number; checked?: boolean }[];
}
