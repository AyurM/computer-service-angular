import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent implements OnInit {
  @Input() paginationInfo: { maxPage: number; pages: number[] };
  @Input() currentPage: number;
  @Output() pageSelectEvent = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}

  onPageSelect(page: number) {
    this.pageSelectEvent.emit(page);
  }
}
