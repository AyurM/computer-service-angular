import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../product.model';

@Component({
  selector: 'app-product-list-item',
  templateUrl: './product-list-item.component.html',
  styleUrls: ['./product-list-item.component.scss'],
})
export class ProductListItemComponent implements OnInit {
  @Input() product: Product;

  MAX_DESCRIPTION_LENGTH = 64;

  constructor() {}

  ngOnInit(): void {}

  getProductDescription(): string {
    if (this.product.attributes.length === 0) {
      return this.product.description;
    }

    let result = '';
    for (const attribute of this.product.attributes) {
      if (
        result.length + attribute.attrValue.length <=
        this.MAX_DESCRIPTION_LENGTH
      ) {
        result += attribute.attrValue + ', ';
      } else {
        return this.trimDesription(result);
      }
    }

    return this.trimDesription(result);
  }

  private trimDesription(str: string) {
    const trimIndex = str.lastIndexOf(',');
    if (trimIndex !== -1) {
      return str.substring(0, trimIndex);
    }
    return str;
  }
}
