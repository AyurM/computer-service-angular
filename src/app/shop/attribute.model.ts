export interface Attribute {
  id: number;
  name: string;
  attributeGroupId: number;
  attrGroupName: string;
  attrValue: string;
}
