import { Directive, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[appDropdown]',
})
export class DropdownDirective {
  @HostBinding('class.open') isOpen = false;

  @HostListener('click', ['$event']) toggleOpen(event: Event) {
    this.isOpen = !this.isOpen;
    event.stopPropagation();
  }

  @HostListener('document:click', ['$event']) outsideClick(event: Event) {
    if (this.isOpen) {
      this.isOpen = false;
    }
  }
}
