import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Product } from '../product.model';
import { ProductCompareService } from '../../services/product-compare.service';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss'],
})
export class ProductCardComponent implements OnInit, OnDestroy {
  @Input() product: Product;
  @Input() categoryInfo: string;

  compareHover = false;
  addedToCompare = false;

  private MAX_DESCRIPTION_LENGTH = 64;
  private compareServiceSub: Subscription;

  constructor(
    private compareService: ProductCompareService,
    private cartService: CartService
  ) {}

  ngOnInit(): void {
    this.compareServiceSub = this.compareService.productsSubject.subscribe(
      () => {
        this.addedToCompare = this.compareService.hasProduct(
          this.categoryInfo,
          this.product
        );
      }
    );
  }

  getProductDescription(): string {
    if (this.product.attributes.length === 0) {
      return this.product.description;
    }

    let result = '';
    for (const attribute of this.product.attributes) {
      if (
        result.length + attribute.attrValue.length <=
        this.MAX_DESCRIPTION_LENGTH
      ) {
        result += attribute.attrValue + ', ';
      } else {
        return this.trimDesription(result);
      }
    }

    return this.trimDesription(result);
  }

  onCompareMouseIn() {
    this.compareHover = true;
  }

  onCompareMouseOut() {
    this.compareHover = false;
  }

  onAddToCompare() {
    if (this.addedToCompare) {
      this.compareService.removeProduct(this.product, this.categoryInfo);
    } else {
      this.compareService.addProduct(this.product, this.categoryInfo);
    }
  }

  onAddToCart() {
    this.cartService.addProduct(this.product, 1);
  }

  private trimDesription(str: string) {
    const trimIndex = str.lastIndexOf(',');
    if (trimIndex !== -1) {
      return str.substring(0, trimIndex);
    }
    return str;
  }

  ngOnDestroy() {
    this.compareServiceSub.unsubscribe();
  }
}
