import { Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

import { ProductCompareService } from '../../../services/product-compare.service';
import { BreadcrumbService } from '../../../services/breadcrumb.service';
import { Product } from '../../product.model';
import {
  FilterAttributeGroup,
  FilterValuesResponse,
} from '../../filter/filter.model';

@Component({
  selector: 'app-compare-page',
  templateUrl: './compare-page.component.html',
  styleUrls: ['./compare-page.component.scss'],
})
export class ComparePageComponent implements OnInit, OnDestroy {
  products: Map<string, Product[]>;
  categories: { id: number; name: string; items: number }[] = [];
  attrGroups: FilterAttributeGroup[];
  selectedCategoryKey: string;
  selectedCategoryIndex = 0;
  showOnlyDifferences = false;

  constructor(
    private compareService: ProductCompareService,
    private breadcrumbService: BreadcrumbService,
    private pageTitle: Title
  ) {}

  private subs: Subscription[] = [];

  ngOnInit(): void {
    this.pageTitle.setTitle('Сравнение товаров');
    this.products = new Map<string, Product[]>();
    this.breadcrumbService.breadcrumbs = [
      { label: 'Главная', url: '' },
      { label: 'Магазин', url: '/shop' },
      {
        label: 'Сравнение товаров',
        url: '/shop/compare',
      },
    ];

    this.subs.push(
      this.compareService.attributesSubject.subscribe((fvResponse) => {
        this.onFilterValuesResponse(fvResponse);
      })
    );

    this.subs.push(
      this.compareService.productsSubject.subscribe((products) => {
        this.onProductsToCompare(products);
      })
    );
  }

  getAttrValuesFromProducts(attrGroupId: number, attrId: number): string[] {
    const result = [];
    this.products.get(this.selectedCategoryKey).forEach((product) => {
      const attr = product.attributes.find((attrValue) => {
        return (
          attrValue.attributeGroupId === attrGroupId && attrValue.id === attrId
        );
      });
      if (attr) {
        result.push(attr.attrValue);
      } else {
        result.push('-');
      }
    });

    if (this.showOnlyDifferences && result.length > 1) {
      if (
        result.every((el, i, result) => {
          return el === result[0];
        })
      ) {
        return [];
      }
    }
    return result;
  }

  onSelectCategory(index: number) {
    this.compareService.getCategoryAttributes(
      this.categories[index].id.toString()
    );
  }

  onOnlyDifferencesToggle(event: any) {
    this.showOnlyDifferences = event.target.checked;
  }

  onDeleteClick(product: Product) {
    this.compareService.removeProduct(product);
  }

  private onFilterValuesResponse(fvr: FilterValuesResponse) {
    if (!fvr) {
      return;
    }
    this.attrGroups = fvr.attrGroups;
    this.selectedCategoryKey = JSON.stringify({
      id: fvr.id,
      name: fvr.name,
    });
    this.selectedCategoryIndex = this.categories.findIndex((el) => {
      return el.id === fvr.id;
    });
  }

  private onProductsToCompare(products: Map<string, Product[]>) {
    this.products = products;
    this.parseProductsMapKeys(products);
    if (this.categories.length > 0) {
      this.compareService.getCategoryAttributes(
        this.categories[this.selectedCategoryIndex].id.toString()
      );
    }
  }

  private parseProductsMapKeys(products: Map<string, Product[]>) {
    if (this.products.size < this.categories.length) {
      this.selectedCategoryIndex = 0;
    }
    this.categories = [];
    Array.from(products.keys()).forEach((categoryStringInfo, index) => {
      this.categories.push({
        ...JSON.parse(categoryStringInfo),
        items: products.get(categoryStringInfo).length,
      });
      if (index === this.selectedCategoryIndex) {
        this.selectedCategoryKey = categoryStringInfo;
      }
    });
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
