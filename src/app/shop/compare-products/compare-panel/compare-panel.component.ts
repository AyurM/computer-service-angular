import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';

import { ProductCompareService } from '../../../services/product-compare.service';
import { Product } from '../../product.model';
import { Utils } from '../../../services/utils/utils';

@Component({
  selector: 'app-compare-panel',
  templateUrl: './compare-panel.component.html',
  styleUrls: ['./compare-panel.component.scss'],
})
export class ComparePanelComponent implements OnInit, OnDestroy {
  products: Map<string, Product[]>;
  isHidden = false;
  isSlidingDown = false;
  panelTimeouts: number[] = [];

  private productsSub: Subscription;
  private panelStayDuration = 5000;
  private slideDownDuration = 250;

  constructor(
    private elRef: ElementRef,
    private compareService: ProductCompareService
  ) {}

  ngOnInit(): void {
    this.elRef.nativeElement.style.setProperty(
      '--transition-duration',
      `${this.slideDownDuration}ms`
    );
    this.productsSub = this.compareService.productsSubject.subscribe(
      (productsToCompare) => {
        this.isHidden = false;
        this.products = productsToCompare;
        this.clearPanelHideTimeouts();
        this.setPanelHideTimeouts();
      }
    );
  }

  getProductsToCompare(): Product[] {
    return this.compareService.getProductsArray();
  }

  getProductsPlural(): string {
    return Utils.getProductsPlural(this.getProductsToCompare().length);
  }

  onDeleteClick(product: Product) {
    this.compareService.removeProduct(product);
  }

  onClearAll() {
    this.compareService.clearAll();
  }

  onMouseEnter() {
    this.clearPanelHideTimeouts();
  }

  onMouseLeave() {
    this.setPanelHideTimeouts();
  }

  private clearPanelHideTimeouts() {
    this.panelTimeouts.forEach((id) => clearTimeout(id));
    this.panelTimeouts = [];
    this.isSlidingDown = false;
  }

  private setPanelHideTimeouts() {
    this.panelTimeouts.push(
      setTimeout(() => {
        this.isSlidingDown = true;
      }, this.panelStayDuration - this.slideDownDuration)
    );
    this.panelTimeouts.push(
      setTimeout(() => {
        this.isHidden = true;
        this.isSlidingDown = false;
      }, this.panelStayDuration)
    );
  }

  ngOnDestroy() {
    this.productsSub.unsubscribe();
  }
}
