export interface ShopCategory {
  id: number;
  name: string;
  imageUrl: string;
  totalItems: number;
  parentId: number;
}
