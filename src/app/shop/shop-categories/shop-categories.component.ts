import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { ShopCategory } from '../shop-category.model';
import { ShopService } from '../../services/shop.service';
import { Utils } from '../../services/utils/utils';

@Component({
  selector: 'app-shop-categories',
  templateUrl: './shop-categories.component.html',
  styleUrls: ['./shop-categories.component.scss'],
})
export class ShopCategoriesComponent implements OnInit, OnDestroy {
  @Input() categoryId: string;
  categories: ShopCategory[] = [];
  isLoading: boolean = true;

  private serviceSubscriptions: Subscription[] = [];

  constructor(private router: Router, private shopService: ShopService) {}

  ngOnInit(): void {
    this.serviceSubscriptions.push(
      this.shopService.isCategoriesLoadingSubject.subscribe((loadingStatus) => {
        this.isLoading = loadingStatus;
      })
    );
    this.serviceSubscriptions.push(
      this.shopService.categoriesSubject.subscribe((fetchedCategories) => {
        this.categories = fetchedCategories;
      })
    );
    this.serviceSubscriptions.push(
      this.shopService.errorSubject.subscribe((error) => {
        console.log(error);
        this.router.navigate(['/error'], {
          state: { status: error.status, message: error.message },
        });
      })
    );

    if (!this.categoryId) {
      this.shopService.requestMainCategories();
    } else {
      this.shopService.requestSubcategories(this.categoryId);
    }
  }

  hasSubcategories(category: ShopCategory): boolean {
    return this.categories.some((element) => {
      return element.parentId === category.id;
    });
  }

  getSubcategories(category: ShopCategory): ShopCategory[] {
    return this.categories.filter((element) => {
      return element.parentId === category.id;
    });
  }

  getCategoriesToDisplay(): ShopCategory[] {
    if (this.hasRootCategories(this.categories)) {
      return this.categories.filter((element) => {
        return element.parentId === 0;
      });
    } else {
      return this.categories;
    }
  }

  getProductsString(totalProducts: number): string {
    return Utils.getProductsPlural(totalProducts);
  }

  private hasRootCategories(categoriesArray: ShopCategory[]): boolean {
    return categoriesArray.some((element) => {
      return element.parentId === 0;
    });
  }

  ngOnDestroy() {
    this.serviceSubscriptions.forEach((sub) => {
      sub.unsubscribe();
    });
  }
}
