import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Params } from '@angular/router';

import { BreadcrumbService } from '../../services/breadcrumb.service';

@Component({
  selector: 'app-shop-page',
  templateUrl: './shop-page.component.html',
  styleUrls: ['./shop-page.component.scss'],
})
export class ShopPageComponent implements OnInit {
  categoryId: string;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private route: ActivatedRoute,
    private pageTitle: Title
  ) {}

  ngOnInit(): void {
    this.pageTitle.setTitle('Магазин радиодеталей и комплектующих');
    this.route.params.subscribe((params: Params) => {
      this.categoryId = params['id'];
    });
    this.breadcrumbService.breadcrumbs = [
      { label: 'Главная', url: '' },
      { label: 'Магазин', url: '/shop' },
    ];
  }
}
