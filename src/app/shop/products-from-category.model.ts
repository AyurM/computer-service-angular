import { Product } from './product.model';

//Interface for "/api/product/category/:id" and
//"/api/product/filter" endpoints
export interface ProductsFromCategory {
  id: number;
  name: string;
  totalItems: number;
  minPrice: number;
  maxPrice: number;
  page: number;
  products: Product[];
}
