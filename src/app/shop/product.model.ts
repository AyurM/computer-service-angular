import { Attribute } from './attribute.model';

export interface Product {
  id: number;
  name: string;
  price: number;
  discountPrice: number;
  imageUrl: string;
  available: number;
  description: string;
  manufacturerName: string;
  category: { id: number; name: string };
  attributes: Attribute[];
}
