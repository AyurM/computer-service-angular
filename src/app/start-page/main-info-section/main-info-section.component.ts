import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-main-info-section',
  templateUrl: './main-info-section.component.html',
  styleUrls: ['./main-info-section.component.scss'],
})
export class MainInfoSectionComponent implements OnInit {
  @Input() sectionTitle: string;
  @Input() sectionDescription: string;
  @Input() sectionImageUrl: string;
  @Input() textOnTheLeft: boolean;

  constructor() {}

  ngOnInit(): void {}
}
