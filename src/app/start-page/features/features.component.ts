import { Component, OnInit } from '@angular/core';
import { Feature } from './feature.model';

@Component({
  selector: 'app-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.scss'],
})
export class FeaturesComponent implements OnInit {
  features: Feature[] = [
    {
      number: 15,
      unit: 'лет',
      text: 'на рынке услуг',
    },
    {
      number: 100,
      unit: 'Тб',
      text: 'данных восстановлено',
    },
    {
      number: 12000,
      unit: 'ед.',
      text: 'техники отремонтировано',
    },
    {
      number: 6000,
      unit: '',
      text: 'довольных клиентов',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
