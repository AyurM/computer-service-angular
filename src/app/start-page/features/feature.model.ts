export interface Feature {
  number: number;
  unit: string;
  text: string;
}
