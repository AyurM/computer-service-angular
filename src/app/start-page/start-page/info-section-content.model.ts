export interface InfoSectionContent {
  title: string;
  description: string;
  imageUrl: string;
}
