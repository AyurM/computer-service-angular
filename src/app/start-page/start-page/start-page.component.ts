import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { InfoSectionContent } from './info-section-content.model';

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.scss'],
})
export class StartPageComponent implements OnInit {
  sections: InfoSectionContent[] = [
    {
      title: 'Многолетний опыт ремонта компьютерной техники',
      description: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo cupiditate
      corrupti voluptates, iure nulla veniam tempore. Obcaecati, voluptatem.
      Excepturi vero pariatur doloremque aspernatur dolorum.`,
      imageUrl: 'assets/images/computer_repair.webp',
    },
    {
      title: 'Единственный в Улан-Удэ сервис по восстановлению данных',
      description: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo cupiditate
      corrupti voluptates, iure nulla veniam tempore. Obcaecati, voluptatem.
      Excepturi vero pariatur doloremque aspernatur dolorum.`,
      imageUrl: 'assets/images/data_restore.webp',
    },
  ];

  constructor(private pageTitle: Title) {}

  ngOnInit(): void {
    this.pageTitle.setTitle(
      'Сервис-центр - ремонт компьютерной техники, восстановление данных, радиодетали'
    );
  }
}
