import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { BreadcrumbService } from '../services/breadcrumb.service';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss'],
})
export class BreadcrumbsComponent implements OnInit, OnDestroy {
  breadcrumbs: { label: string; url: string }[];

  private sub: Subscription;

  constructor(
    private router: Router,
    private breadcrumbService: BreadcrumbService
  ) {}

  ngOnInit(): void {
    this.sub = this.breadcrumbService.breadcrumbSubject.subscribe(
      (breadcrumbs) => {
        this.breadcrumbs = breadcrumbs;
      }
    );
    this.router.events
      .pipe(filter((event) => event instanceof NavigationEnd))
      .subscribe((event) => {
        this.breadcrumbs = [];
      });
  }

  private buildBreadcrumbs(routeData: any): { label: string; url: string }[] {
    if (!routeData) {
      return [];
    }
    return routeData;
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
