import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

import {
  FilterValuesResponse,
  FilterAttribute,
  FilterCriteria,
} from '../shop/filter/filter.model';
import { SortOptions, SortOptionsMap } from '../shop/filter/filter-enums';
import { Utils } from '../services/utils/utils';

@Injectable({
  providedIn: 'root',
})
export class FilterService {
  private _filterAttributesSubject = new BehaviorSubject<FilterAttribute[]>([]);
  private filterAttributes: FilterAttribute[];

  private _priceRangeSubject = new Subject<{
    minPrice: number;
    maxPrice: number;
  }>();
  private _priceRange: { minPrice: number; maxPrice: number } = {
    minPrice: 0,
    maxPrice: 0,
  };

  private _searchQuery = '';
  private _searchQuerySubject = new BehaviorSubject<string>(this._searchQuery);

  private _sortBy: SortOptions = SortOptions.PriceAsc;
  private _pageNumber = 1;
  private _productsPerPage = 6;
  private _productListIndices = { start: 1, end: 1 };
  private _paginationInfo: { maxPage: number; pages: number[] } = {
    maxPage: 1,
    pages: [],
  };

  constructor() {}

  get filterAttributesSubject(): Subject<FilterAttribute[]> {
    return this._filterAttributesSubject;
  }

  get priceRangeSubject(): Subject<{ minPrice: number; maxPrice: number }> {
    return this._priceRangeSubject;
  }

  get priceRange(): { minPrice: number; maxPrice: number } {
    return this._priceRange;
  }

  get searchQuerySubject(): Subject<string> {
    return this._searchQuerySubject;
  }

  get pageNumber(): number {
    return this._pageNumber;
  }

  set pageNumber(newNumber: number) {
    this._pageNumber = newNumber > 0 ? newNumber : 1;
  }

  get productsPerPage(): number {
    return this._productsPerPage;
  }

  get productListIndices(): { start: number; end: number } {
    return this._productListIndices;
  }

  get paginationInfo(): { maxPage: number; pages: number[] } {
    return this._paginationInfo;
  }

  set minPrice(price: number) {
    this._priceRange.minPrice = price < 0 ? 0 : price;
    this.checkPriceRangeMargins();
    this._priceRangeSubject.next(this._priceRange);
  }

  set maxPrice(price: number) {
    this._priceRange.maxPrice = price < 0 ? 0 : price;
    this.checkPriceRangeMargins();
    this._priceRangeSubject.next(this._priceRange);
  }

  set searchQuery(query: string) {
    this._searchQuery = query;
  }

  getSelectedSortByOption(): number {
    return Object.values(SortOptions).indexOf(this._sortBy);
  }

  setSortByOption(option: number) {
    this._sortBy = Object.values(SortOptions)[option];
  }

  getSortOptionNames(): string[] {
    const result = [];
    Object.values(SortOptions).forEach((sortOption) => {
      result.push(SortOptionsMap.get(sortOption));
    });
    return result;
  }

  onApplyFilter() {
    this._pageNumber = 1;
    this._filterAttributesSubject.next(this.filterAttributes);
    this._searchQuerySubject.next(this._searchQuery);
    this._priceRangeSubject.next(this._priceRange);
  }

  resetFilter() {
    this._pageNumber = 1;
    this.resetSearchQuery();
    this.resetPriceRange();
    this.resetOptions();
  }

  refreshFilterData(childFilterData: FilterAttribute) {
    for (var i = 0; i < this.filterAttributes.length; i++) {
      if (this.filterAttributes[i].id === childFilterData.id) {
        this.filterAttributes[i] = childFilterData;
        break;
      }
    }
  }

  removeFilterOption(removedOption: {
    id: number;
    name: string;
    value: string;
  }) {
    for (let i = 0; i < this.filterAttributes.length; i++) {
      if (this.filterAttributes[i].id === removedOption.id) {
        for (let j = 0; j < this.filterAttributes[i].values.length; j++) {
          if (
            this.filterAttributes[i].values[j].value === removedOption.value
          ) {
            this.filterAttributes[i].values[j].checked = false;
            break;
          }
        }
        break;
      }
    }
  }

  extractFilterAttributesAndManufacturers(filterValues: FilterValuesResponse) {
    const filterAttributes: FilterAttribute[] = [];
    filterAttributes.push({
      id: 0,
      name: 'Производитель',
      values: filterValues.manufacturers,
    });
    filterValues.attrGroups.forEach((attrGroup) => {
      attrGroup.attributes.forEach((attribute) => {
        filterAttributes.push(attribute);
      });
    });

    this.filterAttributes = filterAttributes;
    this._filterAttributesSubject.next(filterAttributes);
  }

  getFilterCriteria(categoryId: number): FilterCriteria {
    const filterCriteria: FilterCriteria = {
      page: this._pageNumber,
      itemsperpage: this.productsPerPage,
      categoryId: categoryId,
      filterData: [],
      sortBy: this._sortBy,
    };

    filterCriteria.filterData.push(...this.prepareFilterData());
    if (this.isPriceRangeSpecified()) {
      filterCriteria.priceRange = this._priceRange;
    }
    if (this._searchQuery !== '') {
      filterCriteria.searchQuery = this._searchQuery;
    }
    return filterCriteria;
  }

  updatePaginationInfo(totalItems: number) {
    this._paginationInfo = Utils.calcPages(
      totalItems,
      this._pageNumber,
      this._productsPerPage
    );
  }

  updateListIndices(totalItems: number) {
    this._productListIndices.start =
      (this._pageNumber - 1) * this._productsPerPage + 1;
    this._productListIndices.end =
      this._pageNumber === this._paginationInfo.maxPage
        ? totalItems
        : this._pageNumber * this._productsPerPage;
  }

  private prepareFilterData(): { attributeId: number; values: string[] }[] {
    const result = [];

    this.filterAttributes.forEach((attr) => {
      const filterValues = [];
      attr.values.forEach((value) => {
        if (value.checked) {
          filterValues.push(value.value);
        }
      });
      if (filterValues.length > 0) {
        result.push({
          attributeId: attr.id,
          values: filterValues,
        });
      }
    });

    return result;
  }

  private isPriceRangeSpecified(): boolean {
    return !(
      this._priceRange.minPrice === 0 && this._priceRange.maxPrice === 0
    );
  }

  private checkPriceRangeMargins() {
    if (
      this._priceRange.maxPrice < this._priceRange.minPrice &&
      this._priceRange.minPrice > 0 &&
      this._priceRange.maxPrice > 0
    ) {
      let temp = this._priceRange.minPrice;
      this._priceRange.minPrice = this._priceRange.maxPrice;
      this._priceRange.maxPrice = temp;
    }
  }

  private resetPriceRange() {
    this._priceRange.minPrice = 0;
    this._priceRange.maxPrice = 0;
    this._priceRangeSubject.next(this._priceRange);
  }

  private resetOptions() {
    this._sortBy = SortOptions.PriceAsc;
  }

  private resetSearchQuery() {
    this._searchQuery = '';
    this._searchQuerySubject.next(this._searchQuery);
  }
}
