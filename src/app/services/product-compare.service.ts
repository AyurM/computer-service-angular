import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Subject } from 'rxjs';

import { environment } from '../../environments/environment';

import { Product } from '../shop/product.model';
import { FilterValuesResponse } from '../shop/filter/filter.model';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root',
})
export class ProductCompareService {
  private products: Map<string, Product[]> = new Map<string, Product[]>();
  private _productsSubject = new BehaviorSubject<Map<string, Product[]>>(
    this.products
  );

  private categoryAttributes: FilterValuesResponse;
  private _categoryAttrsSubject = new Subject<FilterValuesResponse>();

  constructor(private http: HttpClient) {}

  get productsSubject(): BehaviorSubject<Map<string, Product[]>> {
    return this._productsSubject;
  }

  get attributesSubject(): Subject<FilterValuesResponse> {
    return this._categoryAttrsSubject;
  }

  addProduct(product: Product, productCategory: string) {
    if (this.products.has(productCategory)) {
      this.products.get(productCategory).push(product);
    } else {
      this.products.set(productCategory, [product]);
    }
    this._productsSubject.next(this.products);
  }

  removeProduct(product: Product, productCategory?: string) {
    if (!productCategory) {
      this.removeProductWithoutCategoryId(product);
      return;
    }

    const index = this.getProductIndexWithinCategory(productCategory, product);
    if (index === -1) {
      return;
    }
    this.products.get(productCategory).splice(index, 1);
    if (this.products.get(productCategory).length === 0) {
      this.products.delete(productCategory);
    }
    this._productsSubject.next(this.products);
  }

  hasProduct(productCategory: string, product: Product): boolean {
    return this.getProductIndexWithinCategory(productCategory, product) !== -1;
  }

  getProductsArray(): Product[] {
    if (!this.products) {
      return [];
    }
    const result = [];
    this.products.forEach((products) => {
      result.push(...products);
    });
    return result;
  }

  clearAll() {
    this.products.clear();
    this._productsSubject.next(this.products);
  }

  getCategoryAttributes(categoryId: string) {
    this.http
      .get<FilterValuesResponse>(API_URL + `/shop/filter_values/${categoryId}`)
      .subscribe(
        (result) => {
          this.categoryAttributes = result;
          this._categoryAttrsSubject.next(this.categoryAttributes);
        },
        (error) => {
          this.categoryAttributes = null;
          this._categoryAttrsSubject.next(this.categoryAttributes);
        }
      );
  }

  private getProductIndexWithinCategory(
    productCategory: string,
    product: Product
  ): number {
    if (!this.products.has(productCategory)) {
      return -1;
    }
    return this.products.get(productCategory).findIndex((element) => {
      return element.id === product.id;
    });
  }

  private removeProductWithoutCategoryId(product: Product) {
    for (const key of this.products.keys()) {
      if (this.getProductIndexWithinCategory(key, product) !== -1) {
        this.removeProduct(product, key);
        break;
      }
    }
  }
}
