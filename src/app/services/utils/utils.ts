import { HttpParams } from '@angular/common/http';
import { FilterCriteria } from '../../shop/filter/filter.model';

export class Utils {
  static getParamsFromFilterCriteria(
    filterCriteria: FilterCriteria
  ): HttpParams {
    let result = new HttpParams();
    result = result.set('category', filterCriteria.categoryId.toString());
    result = result.set('page', filterCriteria.page.toString());
    result = result.set('itemsperpage', filterCriteria.itemsperpage.toString());
    result = this.addPriceRangeParams(filterCriteria, result);
    result = this.addAttributeParams(filterCriteria, result);
    result = this.addSortByOption(filterCriteria, result);
    if (filterCriteria.searchQuery) {
      result = result.set('query', filterCriteria.searchQuery);
    }

    return result;
  }

  private static addPriceRangeParams(
    filterCriteria: FilterCriteria,
    params: HttpParams
  ): HttpParams {
    if (filterCriteria.priceRange?.minPrice > 0) {
      params = params.set(
        'minprice',
        filterCriteria.priceRange.minPrice.toString()
      );
    }
    if (filterCriteria.priceRange?.maxPrice > 0) {
      params = params.set(
        'maxprice',
        filterCriteria.priceRange.maxPrice.toString()
      );
    }
    return params;
  }

  static calcPages(
    totalItems: number,
    currentPage: number,
    productsPerPage: number
  ): { maxPage: number; pages: number[] } {
    let result = {
      maxPage: Math.ceil(totalItems / productsPerPage),
      pages: [],
    };
    for (let i = -2; i <= 2; i++) {
      let index = currentPage + i;
      if (index > 0 && index <= result.maxPage) {
        result.pages.push(index);
      }
    }

    return result;
  }

  static getProductsPlural(products: number): string {
    let remainder100 = products % 100;
    if (
      remainder100 == 11 ||
      remainder100 == 12 ||
      remainder100 == 13 ||
      remainder100 == 14
    )
      return 'товаров';

    let remainder = products % 10;
    if (remainder == 1) return 'товар';
    if (remainder == 2 || remainder == 3 || remainder == 4) return 'товара';

    return 'товаров';
  }

  private static addAttributeParams(
    filterCriteria: FilterCriteria,
    params: HttpParams
  ): HttpParams {
    filterCriteria.filterData.forEach((data) => {
      const queryParam = this.getAttributeQueryParam(data);
      params = params.set(queryParam.key, queryParam.value);
    });
    return params;
  }

  private static getAttributeQueryParam(filterAttr: {
    attributeId: number;
    values: string[];
  }): { key: string; value: string } {
    const resultKey =
      filterAttr.attributeId === 0
        ? 'manufacturer'
        : `attr${filterAttr.attributeId}`;
    const resultValue = filterAttr.values.join(',');
    return { key: resultKey, value: resultValue };
  }

  private static addSortByOption(
    filterCriteria: FilterCriteria,
    params: HttpParams
  ): HttpParams {
    if (!filterCriteria.sortBy) {
      return params;
    }

    params = params.set('sortby', filterCriteria.sortBy);
    return params;
  }
}
