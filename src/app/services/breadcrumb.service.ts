import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { Breadcrumb } from '../breadcrumbs/breadcrumb.model';

@Injectable({
  providedIn: 'root',
})
export class BreadcrumbService {
  private _breadcrumbSubject = new Subject<Breadcrumb[]>();
  private _breadcrumbs: Breadcrumb[] = [];

  constructor() {}

  get breadcrumbSubject(): Subject<Breadcrumb[]> {
    return this._breadcrumbSubject;
  }

  set breadcrumbs(bcrumbs: Breadcrumb[]) {
    this._breadcrumbs = bcrumbs;
    this._breadcrumbSubject.next(this._breadcrumbs);
  }
}
