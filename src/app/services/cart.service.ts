import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { Product } from '../shop/product.model';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  private products: Map<Product, number> = new Map<Product, number>();

  private _productsSubject = new BehaviorSubject<Map<Product, number>>(
    this.products
  );

  constructor() {}

  get size(): number {
    return this.products.size;
  }

  get productsSubject(): BehaviorSubject<Map<Product, number>> {
    return this._productsSubject;
  }

  addProduct(product: Product, quantity: number) {
    const productKey = this.getProductKey(product.id);
    if (productKey) {
      const newQuantity = this.products.get(productKey) + quantity;
      if (newQuantity <= 0) {
        this.removeProduct(product);
        return;
      } else {
        this.products.set(productKey, newQuantity);
      }
    } else {
      this.products.set(product, quantity);
    }

    this._productsSubject.next(this.products);
  }

  removeProduct(product: Product) {
    const productKey = this.getProductKey(product.id);
    this.products.delete(productKey);
    this._productsSubject.next(this.products);
  }

  hasProduct(productId: number): boolean {
    return (
      Array.from(this.products.keys()).findIndex((key) => {
        return key.id === productId;
      }) !== -1
    );
  }

  private getProductKey(productId: number): Product {
    return Array.from(this.products.keys()).find((key) => {
      return key.id === productId;
    });
  }
}
