import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

import { environment } from '../../environments/environment';

import { FilterService } from './filter.service';
import { ShopCategory } from '../shop/shop-category.model';
import { ProductsFromCategory } from '../shop/products-from-category.model';
import {
  FilterValuesResponse,
  FilterCriteria,
} from '../shop/filter/filter.model';
import { AppError } from '../error-page/error.model';
import { Utils } from '../services/utils/utils';
import { Product } from '../shop/product.model';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root',
})
export class ShopService {
  private fetchedCategoriesSubject = new Subject<ShopCategory[]>();
  private categorySubject = new Subject<ShopCategory>();
  private fetchedProductsSubject = new Subject<ProductsFromCategory>();
  private productSubject = new Subject<Product>();

  private isProductsLoading = new Subject<boolean>();
  private isFilterLoading = new Subject<boolean>();
  private isCategoriesLoading = new Subject<boolean>();
  private error = new Subject<AppError>();

  private productsInfo: ProductsFromCategory;
  private categories: ShopCategory[];

  constructor(private http: HttpClient, private filterService: FilterService) {}

  get categoriesSubject(): Subject<ShopCategory[]> {
    return this.fetchedCategoriesSubject;
  }

  get categoryDetailsSubject(): Subject<ShopCategory> {
    return this.categorySubject;
  }

  get productsSubject(): Subject<ProductsFromCategory> {
    return this.fetchedProductsSubject;
  }

  get productDetailsSubject(): Subject<Product> {
    return this.productSubject;
  }

  get isProductsLoadingSubject(): Subject<boolean> {
    return this.isProductsLoading;
  }

  get isFilterLoadingSubject(): Subject<boolean> {
    return this.isFilterLoading;
  }

  get isCategoriesLoadingSubject(): Subject<boolean> {
    return this.isCategoriesLoading;
  }

  get errorSubject(): Subject<AppError> {
    return this.error;
  }

  //GET list of product categories for main shop page from API
  requestMainCategories() {
    this.isCategoriesLoading.next(true);
    this.http.get<ShopCategory[]>(API_URL + '/shop/categories').subscribe(
      (categories) => {
        this.isCategoriesLoading.next(false);
        this.categories = categories;
        this.fetchedCategoriesSubject.next(categories);
      },
      (error) => {
        this.isCategoriesLoading.next(false);
        this.onError(error);
      }
    );
  }

  //GET list of subcategories for the product category with id = categoryId
  requestSubcategories(categoryId: string) {
    this.isCategoriesLoading.next(true);
    this.http
      .get<ShopCategory[]>(API_URL + `/shop/subcategories/${categoryId}`)
      .subscribe(
        (categories) => {
          this.isCategoriesLoading.next(false);
          this.categories = categories;
          this.fetchedCategoriesSubject.next(categories);
        },
        (error) => {
          this.isCategoriesLoading.next(false);
          this.onError(error);
        }
      );
  }

  requestCategory(categoryId: string) {
    const loadedCategory = this.tryToFindLoadedCategory(categoryId);
    if (loadedCategory) {
      this.categoryDetailsSubject.next(loadedCategory);
      return;
    }

    this.isCategoriesLoading.next(true);
    this.http
      .get<ShopCategory>(API_URL + `/shop/category/${categoryId}`)
      .subscribe(
        (fetchedCategory) => {
          this.isCategoriesLoading.next(false);
          this.categoryDetailsSubject.next(fetchedCategory);
        },
        (error) => {
          this.isCategoriesLoading.next(false);
          this.onError(error);
        }
      );
  }

  requestProduct(productId: string) {
    const loadedProduct = this.tryToFindLoadedProduct(productId);
    if (loadedProduct) {
      this.productDetailsSubject.next(loadedProduct);
      return;
    }

    this.isProductsLoading.next(true);
    this.http.get<Product>(API_URL + `/product/id/${productId}`).subscribe(
      (fetchedProduct) => {
        this.isProductsLoading.next(false);
        this.productDetailsSubject.next(fetchedProduct);
      },
      (error) => {
        this.isProductsLoading.next(false);
        this.onError(error);
      }
    );
  }

  //GET list of products from the category with id = categoryId
  requestProductsFromCategory(categoryId: string) {
    this.filterService.resetFilter();
    this.isProductsLoading.next(true);
    this.http
      .get<ProductsFromCategory>(
        API_URL +
          `/product/category/${categoryId}?page=${this.filterService.pageNumber}&itemsperpage=${this.filterService.productsPerPage}`
      )
      .subscribe(
        (result) => {
          this.onProductsFetch(result);
          this.requestFilterValuesForCategory(result.id.toString());
        },
        (error) => {
          this.isProductsLoading.next(false);
          this.onError(error);
        }
      );
  }

  //GET products filter data to get list of matching products
  requestFilteredProducts(filterCriteria: FilterCriteria) {
    this.isProductsLoading.next(true);
    this.http
      .get<ProductsFromCategory>(API_URL + '/product/filter', {
        params: Utils.getParamsFromFilterCriteria(filterCriteria),
      })
      .subscribe(
        (filteredProducts) => {
          this.onProductsFetch(filteredProducts);
        },
        (error) => {
          this.isProductsLoading.next(false);
          this.onError(error);
        }
      );
  }

  //GET list of filter attributes and their values (including manufacturers)
  //for products from the category with id = categoryId
  requestFilterValuesForCategory(categoryId: string) {
    this.isFilterLoading.next(true);
    this.http
      .get<FilterValuesResponse>(API_URL + `/shop/filter_values/${categoryId}`)
      .subscribe(
        (result) => {
          this.isFilterLoading.next(false);
          this.filterService.extractFilterAttributesAndManufacturers(result);
        },
        (error) => {
          this.isFilterLoading.next(false);
          this.onError(error);
        }
      );
  }

  changePage(pageNumber: number) {
    this.filterService.pageNumber = pageNumber;
    this.requestFilteredProducts(
      this.filterService.getFilterCriteria(this.productsInfo.id)
    );
  }

  changeSortingOption(option: number) {
    this.filterService.setSortByOption(option);
    this.applyFilter();
  }

  removeFilterOption(removedOption: {
    id: number;
    name: string;
    value: string;
  }) {
    this.filterService.removeFilterOption(removedOption);
    this.applyFilter();
  }

  applyFilter() {
    this.filterService.onApplyFilter();
    this.requestFilteredProducts(
      this.filterService.getFilterCriteria(this.productsInfo.id)
    );
  }

  resetSearchQuery() {
    this.filterService.searchQuery = '';
    this.applyFilter();
  }

  resetMinPrice() {
    this.filterService.minPrice = 0;
    this.applyFilter();
  }

  resetMaxPrice() {
    this.filterService.maxPrice = 0;
    this.applyFilter();
  }

  resetFilter() {
    this.requestProductsFromCategory(this.productsInfo.id.toString());
  }

  searchProduct() {
    this.applyFilter();
  }

  private tryToFindLoadedProduct(productId: string): Product {
    if (this.productsInfo) {
      return this.productsInfo.products.find((element) => {
        return element.id === +productId;
      });
    }
    return null;
  }

  private tryToFindLoadedCategory(categoryId: string): ShopCategory {
    if (this.categories) {
      return this.categories.find((element) => {
        return element.id === +categoryId;
      });
    }
    return null;
  }

  private onProductsFetch(fetchedProducts: ProductsFromCategory) {
    this.isProductsLoading.next(false);
    this.productsInfo = fetchedProducts;
    this.filterService.updatePaginationInfo(this.productsInfo.totalItems);
    this.filterService.updateListIndices(this.productsInfo.totalItems);
    this.fetchedProductsSubject.next(fetchedProducts);
  }

  private onError(error) {
    const appError: AppError = {
      status: error.error.status,
      message: error.error.errorMessage,
    };
    this.error.next(appError);
    console.log(error);
  }
}
