import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.scss'],
})
export class ErrorPageComponent implements OnInit {
  errorStatus: string;
  errorMessage: string;

  constructor(private router: Router) {
    this.errorStatus = this.router.getCurrentNavigation().extras.state.status;
    this.errorMessage = this.router.getCurrentNavigation().extras.state.message;
  }

  ngOnInit(): void {}
}
