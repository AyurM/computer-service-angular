export interface AppError {
  status: string;
  message: string;
}
